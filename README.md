# README #

### 專案文件 ###

三版測試網址:
http://hsinchu.perfect.tw/home
http://kh.perfect.tw/home
http://taipei.perfect.tw/hom


### 專案資料夾說明 ###

I18nTool > 多語系轉換工具
Template1 > 版一
Template2 > 版二
Template3 > 版三

### 專案建置方法 ###

只能逐版修正，用VSCode打開該版資料夾，使用指令 npm install還原套件

使用指令 ng serve 瀏覽本地開發

修改src/theme{i}/assets/configuration/config.ts 的Debug模式為true

### 專案發布方法 ###

Project Build Cmd
Theme1:
ng build --prod --deploy-url /theme1/
Theme2:
ng build --prod --deploy-url /theme2/
Theme3:
ng build --prod --deploy-url /theme3/

將產生的js全部移動至theme{i}資料夾中

### 測試機位置 ###
 
 35.187.157.30
 
 更新路徑:C:\web\apache-tomcat-8.5.45\web\ROOT\WEB-INF\classes\static