﻿
var gCalendar = {};

gCalendar.GOOGLEAPI_CLIENT_ID = '419266554160-d6hmphbhkvbruhue87lggatmuf11vfdg.apps.googleusercontent.com';
gCalendar.scopes = 'https://www.googleapis.com/auth/calendar';

gCalendar.requestAuthClick = function() {
    gapi.auth.authorize(
        { 
          client_id: gCalendar.GOOGLEAPI_CLIENT_ID,
          scope: gCalendar.scopes,
          immediate: false
        },
        gCalendar.handleAuthResult);
    return false;
}

gCalendar.handleAuthResult = function(authResult) {
    var c = jslib.GetStorage('gCalendar');
    var start = c.start.split('/');
    var end = c.end.split('/');
    // c.start = jslib.replaceAll(c.start,'/','-');
    // c.end = jslib.replaceAll(c.end,'/','-');
    var _s = new Date(Date.UTC(Number(start[0]),(Number(start[1])-1),Number(start[2]), 0, 0, 0));
    var _e = new Date(Date.UTC(Number(end[0]),(Number(end[1])-1),Number(end[2]), 0, 0, 0));
    _e = _e.addHours(24);
    if (authResult && !authResult.error) {
        var myEvent = {
            'summary': c.name,
            //'location': c.addr,
            'start': {
                'dateTime': _s.addHours(-8),
                'timeZone': 'Asia/Taipei'
            },
            'end': {
                'dateTime': _e.addHours(-8),
                'timeZone': 'Asia/Taipei'
            },
            'reminders': {
                'useDefault': false,
                'overrides': [
                  { 'method': 'email', 'minutes': 24 * 60 },
                  { 'method': 'popup', 'minutes': 10 }
                ]
            }
        };
        gCalendar.addEventToCalendar(myEvent);

    } else {

    }
}

gCalendar.addEventToCalendar = function(myEvent) {
    gapi.client.load('calendar', 'v3', function () {
        var request = gapi.client.calendar.events.insert({
            'calendarId': 'primary',	// calendar ID
            "resource": myEvent							// pass event details with api call
        });

        // handle the response from our api call
        request.execute(function (resp) {
            console.log('resp',resp);
            if (resp.status == 'confirmed') {
                if(window['addOK']){
                    window['addOK']();
                }  
            } else {
                if(window['addFail']){
                    window['addFail']();
                }  
            }
        });
    });
}


Date.prototype.addHours= function(h){
    this.setHours(this.getHours()+h);
    return this;
}