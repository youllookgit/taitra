// == 基礎設定檔 == //
export const Config = {
    VERSION : '2020.05.06',  //版本號
    DefaultLang: 'en_US', //預設語系
    localUrl : 'http://taipei.perfect.tw/api/',
    releaseUrl : '/api/',
    //DebugMode : true
    DebugMode : false
  }