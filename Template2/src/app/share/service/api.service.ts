import { Injectable } from '@angular/core';
//import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';

import { LoadingService } from './loading.service';
import { Config } from '../../../theme2/assets/configuration/config';
declare var jslib:any; //呼叫第三方js
declare var $:any;
@Injectable()

export class ApiService {

  constructor(
    private http: HttpClient,
    private loading: LoadingService
  ) { }

  public baseUrl = (Config.DebugMode) ? Config.localUrl : Config.releaseUrl;
  apiPost(url:string,param:any){
    this.loading.setLoading(true);
    var body = (param) ? param : {};
    //自動加入語系參數
    var _lang = jslib.UrlParam()['lang'];

    var isLang = [
    'zh_TW','en_US','zh_CN','ja_JP','de_DE','fr_FR','es_ES','pt_PT','ru_RU',
    'it_IT','ko_KR','pl_PL','ar_SA','vi_VN','mn_MN','tr_TR','bn_BD','fa_IR',
    'in_ID','bg_BG','uk_UA','nl_NL','ms_MY','th_TH','hu_HU','ro_RO'];
    if(typeof _lang != 'undefined'){
      if(isLang.indexOf('_lang') > -1){
        jslib.SetStorage('lang',_lang);
      }
    }else{
      _lang = jslib.GetStorage('lang');
    }
    _lang = (_lang) ? _lang : Config.DefaultLang;
    body = Object.assign(body,{lang:_lang})
    //console.log('[api Resquest]',url,body);
    
    return new Promise((resolve,reject)=>{
        var _url = this.baseUrl + url + jslib.ObjToParam(body);
        //console.log('_url',_url);
        this.http.get(_url).toPromise().then(
          (res : Response) => {
            this.loading.setLoading(false);
            //console.log(url,res);
            var _result = res['_body'] || '';
            if(_result != ''){
              res = JSON.parse(_result);
            }
            if(res['success']){
              this.HashHandel();
              //針對國別特殊處理/getdata/countryList
              if(_url.indexOf('getdata/countryList') > -1){
                if(_lang != 'zh_TW'){
                  var c = res['data'];
                  c.forEach(citem => {
                    citem.name = citem.engName;
                  });
                }
                resolve(res['data']);
              }else{
                resolve(res['data']);
              }
              
            }else{
              reject(res);
            }
          },
          (error: Response) => {
            this.loading.setLoading(false);
            if(error['status'] != 200){
              var _error = {
                url:error['url'],
                status:error['status'],
                errormsg:error['statusText']
              }
              console.log('[API Error]',_error);
              reject(_error);
            }else{
              reject(error.json());
            }
          }
        );
    });
    
  }
  apiPostData(url:string,param:any){
    this.loading.setLoading(true);
    var body = (param) ? param : {};
    //自動加入語系參數
    var _lang = jslib.UrlParam()['lang'];
    if(typeof _lang != 'undefined'){
      console.log('ApiService method apiPostData',_lang);
      jslib.SetStorage('lang',_lang);
    }else{
      _lang = jslib.GetStorage('lang');
    }

    body = Object.assign(body,{lang:_lang})
    console.log('[api Resquest Post]',url,body);
    
    return new Promise((resolve,reject)=>{
     
      var _url = this.baseUrl + url;
      //console.log('_url',_url);
      this.http.post(_url,body).toPromise().then(
        (res : Response) => {
          this.loading.setLoading(false);
          console.log('Result',res);
          var _result = res['_body'] || '';
          if(_result != ''){
            res = JSON.parse(_result);
          }
          if(res['success']){
            resolve(res['data']);
          }else{
            reject(res);
          }
        },
        (error: Response) => {
          this.loading.setLoading(false);
          if(error['status'] != 200){
            var _error = {
              url:error['url'],
              status:error['status'],
              errormsg:error['statusText']
            }
            console.log('[API Error]',_error);
            reject(_error);
          }else{
            reject(error.json());
          }
        }
      );

    });
    
  }
  apiPostFile(url,File:File){
    var _url = this.baseUrl + url;
    this.loading.setLoading(true);
    const formData: FormData = new FormData();
    formData.append('file',File);
    return new Promise((resolve,reject)=>{
      this.http
        .post(_url, formData
      ).toPromise().then(
        (res)=>{
          this.loading.setLoading(false);
          resolve(res);
        },(error)=>{
          this.loading.setLoading(false);
          reject(error);
        }
      )
    });
    
    
  }

  HashHandel(){
    console.log('HashHandel');
    var hash = window.location.hash;
    if(hash != ''){
      setTimeout(()=>{
        var _scrollTo = $(hash);
        if(_scrollTo){
          $('html, body').animate({
            scrollTop: _scrollTo.offset().top
          }, 0);
        }
      },500);
    }
  }
}
