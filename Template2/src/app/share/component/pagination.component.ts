import { Component, OnInit,OnChanges,Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'pagination',
  templateUrl: './pagination.component.html'
})
export class PaginationComponent implements OnChanges {
  @Input() SourceData;
  public nowIndex = 1;
  public perPage = 20;
  public pageList = [];
  @Output() PageData = new EventEmitter();
  constructor(
  ) {
    
  }

  // ngOnInit() {
  //   console.log('PaginationComponent ngOnInit',this.pageList);
  // }
  ngOnChanges() {
    this.pageList = [];
    if(this.SourceData){
      var pageCount = Math.ceil((this.SourceData.length/this.perPage));
      for(var i = 0 ; i < pageCount; i ++){
        this.pageList.push((i+1));
      }
      this.PageDataSet();
    }
  }
  addPage(event){
    event.preventDefault();
    if(this.nowIndex != this.pageList.length && this.nowIndex < this.pageList.length){
      this.nowIndex = this.nowIndex + 1;
      this.PageDataSet();
    }
  }
  lessPage(event){
    event.preventDefault();
    if(this.nowIndex != 1 && this.nowIndex <= this.pageList.length){
      this.nowIndex = this.nowIndex - 1;
      this.PageDataSet();
    }
  }
  setPage(event,pageIndex){
    event.preventDefault();
    if(pageIndex != this.nowIndex){
      this.nowIndex = pageIndex;
      this.PageDataSet();
    }
  }
  PageDataSet(){
    console.log('pageChanged!');
    var _start = 0;
    if(this.nowIndex != 1){
      _start = Number(this.perPage) * (this.nowIndex - 1);
    } 
    var _end = Number((this.perPage) * (this.nowIndex));
    var _PageData = this.SourceData.slice(_start,_end);
    console.log('_PageData',_PageData);
    this.PageData.emit(_PageData);
  }
}
