import { Component, OnInit,OnChanges,Input,Output,EventEmitter,NgZone } from '@angular/core';
import {I18nService} from '../service/i18n.service'
declare var $;
declare var jslib;
@Component({
  selector: 'cookie-box',
  templateUrl: './cookiebox.component.html'
})
export class CookieBoxComponent implements OnInit
{
  public isShow;
  public msg = '';
  public url_info = '';
  constructor(
    public i18n : I18nService
  ) {
    var _isshow = jslib.GetStorage('cookie');
    if(typeof _isshow != 'undefined'){
       this.isShow = _isshow;
    }else{
      this.isShow = true;
    }
  }
  public _class;
  public swiper;
  ngOnInit() {
    if(this.isShow){
      var lang = this.i18n.getLang();
      
      this.msg = lang['home']['cookie'];
      this.url_info = lang['home']['cookieurl'];
    }
  
  }
  Agree(){
    jslib.SetStorage('cookie',false);
    this.isShow = false;
    $('#top').css('bottom','4.5rem');
  }
}
