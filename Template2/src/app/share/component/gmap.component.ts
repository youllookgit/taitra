import { Component, OnInit ,Input} from '@angular/core';
declare var $;
declare var google;
@Component({
  selector: 'gmap',
  templateUrl: './gmap.component.html'
})
export class GmapComponent implements OnInit {
  @Input() position;
  public gmap;
  constructor() { }
  ngOnInit() {

  }
  ismap = false;
  imgUrl = '';
  ngOnChanges() {
    //console.log('position',this.position);
    if(!this.position){
        return false;
    }
    if(this.position['map_type'] == 1){
        this.ismap = true;
        var lat = this.position['latitude'];
        var lng = this.position['longitude'];
        this.CreatMap(lat,lng);
    }else{
        this.imgUrl = this.position['fileID'];
    }
  }
  CreatMap(_lat,_lng){
    var geocoder;
    var map;
	  var markers = [];
    var latitude = _lat;
    var longtitude = _lng;
    geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(latitude, longtitude);
        var myOptions = {
            zoom: 1,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById('gmap'), myOptions);
        map.setCenter(latlng);

        map.setZoom(15);
        var marker = new google.maps.Marker({
                  map: map,
                  position: latlng,
                  draggable:true
              });
              markers.push(marker);
              google.maps.event.addListener(
                  marker,
                  'mouseup',
                  function() {
                      latitude = marker.position.lat();
                      longtitude = marker.position.lng();
                  }
              );
  }
}
