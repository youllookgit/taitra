import { Component, OnInit } from '@angular/core';
import {I18nService} from '../../share/service/i18n.service';
import { ApiService} from '../../share/service/api.service';
import { LoadingService } from '../../share/service/loading.service';
import { AppModule} from '../../app.module'
import { Meta,Title } from '@angular/platform-browser';
declare var jslib:any; //呼叫第三方js

@Component({
  template: ''
})
export class BaseComponent{
  public langCode = '';      //for i18n
  public i18n : I18nService;
  public pop : LoadingService;
  public baseApi : ApiService;
  public titleService: Title;
  public BannerTitle = '';
  public jslib = jslib;
  public meta: Meta;
  constructor(  
   //public i18n : I18nService
  ) {
    //for i18n
    this.pop = AppModule.injector.get(LoadingService);
    this.i18n = AppModule.injector.get(I18nService);
    this.baseApi = AppModule.injector.get(ApiService);
    this.titleService = AppModule.injector.get(Title);
    this.langCode = this.i18n.getLangCode();
    this.i18n.eventUpdated.subscribe((lang)=>{
       this.langCode = lang;
    });
    this.meta = AppModule.injector.get(Meta);
  }

  setMetaTtitle(title){
    this.titleService.setTitle(title);
  }
  setMeta(key,value){
    this.meta.updateTag({name:key,content:value})
  }
}
