import { Component, OnInit } from '@angular/core';
declare var $;
@Component({
  selector: 'app-tool',
  templateUrl: './tool.component.html'
})
export class ToolComponent implements OnInit {
  public Isopen = false;
  constructor() { }
  nowindx = 1;
  ngOnInit() {
  }
  less(index){
    this.nowindx = index;
    $('.content p').css('font-size','80%');
  }
  normal(index){
    this.nowindx = index;
    $('.content p').css('font-size','100%');
  }
  add(index){
    this.nowindx = index;
    $('.content p').css('font-size','120%');
  }
  print(){
    window.print()
  }
}
