import { NgModule }             from '@angular/core';
import { RouterModule, Routes,ExtraOptions } from '@angular/router';

import { HomeComponent }  from './component/home/home.component';
import { ErrorComponent }  from './component/error/error.component';
import { NotFoundComponent }  from './component/notFound/notFound.component';
import { SearchComponent }  from './component/search/search.component';
import { EventListComponent }  from './component/event/eventList.component';
import { EventDetailComponent }  from './component/event/eventDetail.component';
import { eventSignComponent }  from './component/event/eventSign.component';
import { NoteDetailComponent }  from './component/note/noteDetail.component';
import { NewsListComponent }  from './component/news/newsList.component';
import { NewsDetailComponent }  from './component/news/newsDetail.component';
import{ AboutUsComponent } from './component/about/aboutUs.component';
import{ contactUsComponent } from './component/contactUs/contactUs.component';
import{ SitemapComponent } from './component/sitemap/sitemap.component';
import{ TradeComponent } from './component/trade/trade.component';
import{ TradeMissionComponent } from './component/tradeMission/tradeMission.component';
import{ TradeShowComponent } from './component/tradeShow/tradeShow.component';
import{ conferenceComponent } from './component/conference/conference.component';
import{ confDetailComponent } from './component/conference/confDetail.component';
import{ confPostComponent } from './component/conference/confPost.component';
import { AboutTradeComponent } from './component/about/aboutTrade.component';
import { AboutTaitraComponent } from './component/about/aboutTaitra.component';
import{ OurServiceComponent } from './component/about/ourservice.component';
import { CalendarComponent }  from './component/calendar/calendar.component';
import { AnnounceComponent }  from './component/announce/announce.component';
import { AnnounceDetailComponent }  from './component/announce/announceDetail.component';
export const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  // {
  //   path: '',
  //   component: LayoutComponent,
  //   data: {
  //     title: 'Home'
  //   },
  {
    path: 'home',
    component: HomeComponent,
    data:{animation:'fader'}
  },

  {
    path: 'event',
    data:{animation:'fader'},
    children: [
      { path: 'list', component: EventListComponent,data:{animation:'fader'} },
      { path: 'detail', component: EventDetailComponent,data:{animation:'fader'} },
      { path: 'sign', component: eventSignComponent,data:{animation:'fader'} },
    ]
  },
  {
    path: 'note',
    data:{animation:'fader'},
    children: [
      { path: 'detail', component: NoteDetailComponent,data:{animation:'fader'} }
    ]
  },
  {
    path: 'news',
    data:{animation:'fader'},
    children: [
      { path: 'list', component: NewsListComponent,data:{animation:'fader'} },
      { path: 'detail', component: NewsDetailComponent,data:{animation:'fader'} },
    ]
  },
  {
    path: 'conference',
    data:{animation:'fader'},
    children: [
      { path: 'list', component: conferenceComponent,data:{animation:'fader'} },
      { path: 'detail', component: confDetailComponent,data:{animation:'fader'} },
      { path: 'post', component: confPostComponent,data:{animation:'fader'} },
    ]
  },
  {
    path: 'announce',
    data:{animation:'fader'},
    children: [
      { path: 'list', component: AnnounceComponent,data:{animation:'fader'} },
      { path: 'detail', component: AnnounceDetailComponent,data:{animation:'fader'} },
    ]
  },
  {
    path: 'search',
    component: SearchComponent,
    data:{animation:'fader'}
  },
  {
    path: 'office',
    component: OurServiceComponent,
    data:{animation:'fader'}
  },
  {
    path: 'aboutTaitra',
    component: AboutTaitraComponent,
    data:{animation:'fader'}
  },
  {
    path: 'aboutTrade',
    component: AboutTradeComponent,
    data:{animation:'fader'}
  },
  {
    path: 'aboutUs',
    component: AboutUsComponent,
    data:{animation:'fader'}
  },
  {
    path: 'contactUs',
    component: contactUsComponent,
    data:{animation:'fader'}
  },
  {
    path: 'sitemap',
    component: SitemapComponent,
    data:{animation:'fader'}
  },
  {
    path: 'trade',
    component: TradeComponent,
    data:{animation:'fader'}
  },
  {
    path: 'tradeMission',
    component: TradeMissionComponent,
    data:{animation:'fader'}
  },
  {
    path: 'tradeShow',
    component: TradeShowComponent,
    data:{animation:'fader'}
  },
  {
    path: 'calendar',
    component: CalendarComponent,
    data:{animation:'fader'}
  },
  {
    path: '**',
    component: ErrorComponent,
    data:{animation:'fader'}
  },
  {
    path: 'notfound',
    component: NotFoundComponent,
    data:{animation:'fader'}
  }
];
const routerOptions: ExtraOptions = {
  useHash: false,
  anchorScrolling: 'enabled'
};

@NgModule({
  imports: [ RouterModule.forRoot(routes,routerOptions)],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
