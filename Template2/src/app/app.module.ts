// ==angular== //
import { NgModule,Injector } from '@angular/core';
import { BrowserModule} from '@angular/platform-browser';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';

//import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { Title } from '@angular/platform-browser';

// Import components
import { AppComponent } from './app.component';
import { BaseComponent }  from './share/component/base.component';
// Loading Component
import { LoadingComponent } from './share/component/loading.component';
import { PopConfirmComponent } from './share/component/pop-confirm.component';
// Page Component
import { HomeComponent }  from './component/home/home.component';
import { ErrorComponent }  from './component/error/error.component';
import { NotFoundComponent }  from './component/notFound/notFound.component';
import { SearchComponent }  from './component/search/search.component';
import { EventListComponent }  from './component/event/eventList.component';
import { eventSignComponent }  from './component/event/eventSign.component';
import { NoteDetailComponent }  from './component/note/noteDetail.component';
import { EventDetailComponent }  from './component/event/eventDetail.component';
import { NewsListComponent }  from './component/news/newsList.component';
import { NewsDetailComponent }  from './component/news/newsDetail.component';
import { AboutTradeComponent }  from './component/about/aboutTrade.component';
import{ AboutUsComponent } from './component/about/aboutUs.component';
import{ AboutTaitraComponent } from './component/about/aboutTaitra.component';
import{ OurServiceComponent } from './component/about/ourservice.component';

import{ contactUsComponent } from './component/contactUs/contactUs.component';
import{ SitemapComponent } from './component/sitemap/sitemap.component';
import{ TradeComponent } from './component/trade/trade.component';
import{ TradeMissionComponent } from './component/tradeMission/tradeMission.component';
import{ TradeShowComponent } from './component/tradeShow/tradeShow.component';
import{ conferenceComponent } from './component/conference/conference.component';
import{ confDetailComponent } from './component/conference/confDetail.component';
import{ confPostComponent } from './component/conference/confPost.component';
import{CalendarComponent } from './component/calendar/calendar.component';
import{AnnounceComponent} from './component/announce/announce.component';
import{AnnounceDetailComponent} from './component/announce/announceDetail.component';

// Share Module
import { ShareModule } from './share/share.module';
// Pipe
//import { PipeModule } from '../app/share/pipe/pipe.module';
// Directuve
import { UrlHrefDirective } from './share/directive/hrefDirective'
// const Components
const APP_CONTAINERS = [
  BaseComponent,
  HomeComponent,
  ErrorComponent,
  NotFoundComponent,
  EventListComponent,
  EventDetailComponent,
  eventSignComponent,
  NoteDetailComponent,
  NewsListComponent,
  NewsDetailComponent,
  SearchComponent,

  AboutUsComponent,
  AboutTradeComponent,
  AboutTaitraComponent,
  OurServiceComponent,
  
  contactUsComponent,
  SitemapComponent,
  TradeComponent,
  TradeMissionComponent,
  TradeShowComponent,
  conferenceComponent,
  confDetailComponent,
  confPostComponent,
  CalendarComponent,
  AnnounceComponent,
  AnnounceDetailComponent
];
import { AppRoutingModule } from './app-routing.module';
@NgModule({
  imports: [
    // ==angular== //
    //HttpModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ShareModule,
    //PipeModule,
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    LoadingComponent,
    PopConfirmComponent
    //directuve
    //UrlHrefDirective,
  ],
  providers: [
    // TelegramService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  static injector: Injector;//DI 注入結構宣告
  constructor(
    injector: Injector//DI 注入結構宣告
  ) {
    AppModule.injector = injector;//DI 注入結構宣告
  }
}
