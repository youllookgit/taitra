import { Component, OnInit  } from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import { BaseComponent } from '../../share/component/base.component';
declare var tcalendar;
declare var $;
declare var gCalendar;
@Component({
  selector: 'home-component',
  templateUrl: './home.component.html'
})
export class HomeComponent extends BaseComponent implements OnInit  {
  public IsMobile = false;
  constructor(  
    public _sanitizer: DomSanitizer
  ) {
    super();
    if(this.jslib.ClientWidth() < 768){
      this.IsMobile = true;
    }else{
      this.IsMobile = false;
    }
  }
  // relatedlink
  public adBanner_1;
  public adBanner_2;
  public adBanner_3;
  
  ngOnInit() {
    // relatedlink
    this.GetBanner();
    this.GetRelate();
    this.CalendarInit();
    this.GetAnnouce();
    this.GetVideo();
    this.Highlight();
    this.GetSeo();//處理日期及浮動tradeicon
  }

  public nowDate;
  CalendarInit(day = ''){
    console.log('CalendarInit',day);
    //若有指定日期 取UTC 若否 取Locol
    var now = (day != '') ? tcalendar.GetUtCDate(day) : tcalendar.GetLocDate();
    if(day == ''){
      this.nowDate = now;
      this.default.y = this.nowDate[0];
      this.default.m = this.nowDate[1];
      this.default.d = this.nowDate[2];
      this.GetCalendarData(this.nowDate[0],this.nowDate[1]);
    }else{
      var _date = now;
      this.default.y = _date[0];
      this.default.m = _date[1];
      this.default.d = _date[2];
      this.GetCalendarData(this.default.y,this.default.m);
    }
  }

  public EvtFilter = [];
  public EventList;
  GetCalendarData(y,m){
    console.log('GetCalendarData',y,m);
    // relatedlink
    this.baseApi.apiPost('home/calendar',{
      year:y,month:m
    }).then(
      (res)=>{

        this.EventList = res;
        console.log('IgnorFilter',this.IgnorFilter);
        if(this.IgnorFilter){
          this.EvtFilter = this.EventList;
        }else{
          this.EvtFilter = [];
          //日期演算法 判斷是否需畫下底線
          this.EventList.forEach((item)=>{
            var thisdstr = '' + this.default.y;
            thisdstr = thisdstr + '-' +  ((this.default.m > 9) ? this.default.m + '' : '0' + this.default.m);
            thisdstr = thisdstr + '-' + ((this.default.d > 9) ? this.default.d + '' : '0' + this.default.d);

            if(item['start'] == item['end']){
              var _startdstr = this.jslib.replaceAll(item['start'],'/','-');
              var _s = new Date(_startdstr).getTime();
              var _n = new Date(thisdstr).getTime();
              if(_s == _n)
               this.EvtFilter.push(item);
            }else{
              var _startdstr = this.jslib.replaceAll(item['start'],'/','-');
              var _enddstr = this.jslib.replaceAll(item['end'],'/','-');
              var _s = new Date(_startdstr).getTime();
              var _e = new Date(_enddstr).getTime();
              var _n = new Date(thisdstr).getTime();
              var diff_d = (_s <= _n) && (_e >= _n);
              if(diff_d){
                item['diff'] = (_e - _n);
                this.EvtFilter.push(item);
              }
            }
          });
        }
        this.Render(y,m);
      },(err)=>{

      });
  }
  // calendar
  public Years = [];
  public Months = [1,2,3,4,5,6,7,8,9,10,11,12];
  public days = [];
  public locDate = tcalendar.GetLocDate();
  public default = { y : 0, m : 0 , d : 0 };
  Render(y,m){
    var now = tcalendar.CreateLoc(y,m,1);
    //Day Render
    this.days = [];
    //count week
    //iefixed
    var first_y = (this.default.y > 9) ? this.default.y + '' : '0' + this.default.y;
    var first_m = (this.default.m > 9) ? this.default.m + '' : '0' + this.default.m;
    var getfirst = new Date(first_y + '-' + first_m + '-' + '01');
    var default_week = getfirst.getUTCDay();
    //上個月
    var lastmonth_days = tcalendar.daysInMonth(this.default.m - 1, this.default.y);
    var add_d = (lastmonth_days - default_week);
    for(var i =  add_d; i < lastmonth_days; i++){
      this.days.push(
        {
          text : '',//i,
          class:''
        });
    }
    for(var i = 1 ; i <= tcalendar.daysInMonth(this.default.m,this.default.y) ; i++){
      this.days.push(
        {
          text : i,
          class:this.DateClass(this.default.y,this.default.m,i)
        });
    }
    //下個月
    var diff = 7 - (this.days.length % 7);
    for(var j = 1 ; j <= diff ; j++){
      this.days.push(
        {
          text : '',//i,
          class:''
        });
    }
    //補足高度
    if(this.days.length == 35){
      for(var fix = 0 ; fix < 7 ; fix++){
        this.days.push(
          {
            text : '',
            class:''
          });
      }
    }
    //加入scroll事件
    this.ScrollEvent();
  }
  IgnorFilter = true;
  OnDataChange(param){
    this.IgnorFilter = true;
    if(param == '-'){
      this.default.y = (this.default.m - 1 < 1) ? this.default.y - 1 : this.default.y;
      this.default.m = (this.default.m - 1 < 1) ? 12 : this.default.m - 1;
    }
    if(param == '+'){
      this.default.y = (this.default.m + 1 > 12) ? this.default.y + 1 : this.default.y;
      this.default.m = (this.default.m + 1 > 12) ? 1 : this.default.m + 1;
    }
    this.GetCalendarData(this.default.y,this.default.m);
  }
  
  lessYear(){
     this.IgnorFilter = true;
     this.default.y = this.default.y - 1;
     this.GetCalendarData(this.default.y,this.default.m);
     this.OnDataChange('');
  }
  addYear(){
    this.IgnorFilter = true;
    this.default.y = this.default.y + 1;
    this.GetCalendarData(this.default.y,this.default.m);
    this.OnDataChange('');
  }
  DateClass(y,m,d){
    var cn = '';
    if(!this.IgnorFilter){
      if( y == this.default.y &&
        m == this.default.m &&
        d == this.default.d
      ){
        cn = 'act '
      }
    }

    var _date = y 
    + '-' + (Number(m) > 9 ? Number(m) : '0' + Number(m)) 
    + '-' + (Number(d) > 9 ? Number(d) : '0' + Number(d));
    
    if(this.nowDate[0] == y && this.nowDate[1] == m && this.nowDate[2] == d){
      cn = cn + 'evt';
    }
    var isline = '';
    this.EventList.forEach((item)=>{
      var nowdateStr = (this.nowDate[0] + '-' + this.nowDate[1] + '-' + this.nowDate[2]);
      var _std = this.jslib.replaceAll(item['start'],'/','-');
      var _etd = this.jslib.replaceAll(item['end'],'/','-');
      if(this.DateCheck(_std,_etd,_date)){
        isline = 'line';
      }
    });
    return cn + ' ' + isline;
  }
  DateCheck(start,end,checked){
    var d1 = start.split("-");
    var d2 = end.split("-");
    var c = checked.split("-");
    c[1] = (c[1].length == 1) ? '0' + c[1] : c[1];
    c[2] = (c[2].length == 1) ? '0' + c[2] : c[2];
    var from = new Date(Number(d1[0]), Number(d1[1])-1, d1[2]);
    var to   = new Date(Number(d2[0]), Number(d2[1])-1, d2[2]);
    var check = new Date(Number(c[0]), Number(c[1])-1, c[2]);
    var d_fiff= ((check.getTime() >= from.getTime()) && (check.getTime() <= to.getTime()));
    return d_fiff;
  }
  //Is checked >= target
  DateBigThan(target,checked){
    var _t = target.split("-");
    var c = checked.split("-");
    c[1] = (c[1].length == 1) ? '0' + c[1] : c[1];
    c[2] = (c[2].length == 1) ? '0' + c[2] : c[2];
    var _tdate = new Date(Number(_t[0]), Number(_t[1])-1, _t[2]);
    var check = new Date(Number(c[0]), Number(c[1])-1, c[2]);
    var d_fiff= ((check.getTime() >= _tdate.getTime()));
    return d_fiff;
  }
  ScrollEvent(){
    if(this.EvtFilter && this.EventList.length > 0){
      var _i = 0;
      _i = this.EvtFilter.findIndex((_f)=>{
        var _nowd = (this.nowDate[0] + '-' + this.nowDate[1] + '-' + this.nowDate[2]);
        var _start = this.jslib.replaceAll(_f['start'],'/','-');
        var _end = this.jslib.replaceAll(_f['end'],'/','-');
        return (this.DateCheck(_start,_end,_nowd) || this.DateBigThan(_nowd,_end));
      });

      if(_i > 0){
        setTimeout(()=>{
          var _scrolltop = $('.scroll').offset().top;
          var _itemtop = $('.scroll label').eq(_i).offset().top;
          $('.scroll').animate({
            scrollTop: (_itemtop - _scrolltop)
          },250);
        },500);
      }
    }
  }
  public Banner;
  GetBanner(){
    this.baseApi.apiPost('home/banner',{}).then(
      (res)=>{
        if(res){
          this.Banner = res;
        }
       
      },(err)=>{
    });
  }
  RelateShow = false;
  GetRelate(){
    this.baseApi.apiPost('home/relatedlink',{}).then(
      (res)=>{
        if(res['adBanner_1']){
         this.adBanner_1 = res['adBanner_1'];
        }
        if(res['adBanner_2']){
          this.adBanner_2 = res['adBanner_2'];
         }
         if(res['adBanner_3']){
          this.adBanner_3 = res['adBanner_3'];
         }

         this.RelateShow = (
           (this.adBanner_1 && this.adBanner_1.length > 0) ||
           (this.adBanner_2 && this.adBanner_2.length > 0) ||
           (this.adBanner_3 && this.adBanner_3.length > 0)
          );
      },(err)=>{
      });
  }
  public customAnnouce;
  public autoAnnouce;
  GetAnnouce(){
    this.baseApi.apiPost('home/announce',{}).then(
      (res)=>{
        if(res['auto']){
          this.autoAnnouce = res['auto'];
          this.autoAnnouce.forEach(_auto =>{
            _auto['imgurl'] = '';
            if(_auto['img']){
              _auto['imgurl'] = this._sanitizer.bypassSecurityTrustResourceUrl(_auto['img']);
            }
            if(_auto['url']){
              _auto['safeurl'] = this._sanitizer.bypassSecurityTrustResourceUrl(_auto['url']);
            }
          });
        }
        if(res['custom']){
          this.customAnnouce = res['custom'];
          this.customAnnouce.forEach(_custom =>{
            _custom['imgurl'] = '';
            if(_custom['img']){
              _custom['imgurl'] = this._sanitizer.bypassSecurityTrustResourceUrl(_custom['img']);
            }
            if(_custom['url']){
              _custom['safeurl'] = this._sanitizer.bypassSecurityTrustResourceUrl(_custom['url']);
            }else{
              _custom['safeurl'] = '';
            }
          });
        }
       
      },(err)=>{
    });
  } 
  public video;
  GetVideo(){
    this.baseApi.apiPost('home/video',{}).then(
      (res)=>{
        if(res && res['video']){
          res['video'] = res['video'].replace('https://youtu.be/','https://www.youtube.com/embed/');
          res['url'] = this._sanitizer.bypassSecurityTrustResourceUrl(res['video']);
        }
        this.video = res;
      },(err)=>{
      });
  }
  hlist = [];
  Highlight(){
    this.baseApi.apiPost('getdata/highlight',{}).then(
      (res:any)=>{
        this.hlist = [];
        if(res && res.length > 0){
          this.hlist = res;
        }
      });
  }
  ChangeDate(day){
    this.IgnorFilter = false;
    if(day['class'].indexOf('line') > 0){
      var dstr = '' + this.default.y;
      dstr = dstr + '-' +  ((this.default.m > 9) ? this.default.m + '' : '0' + this.default.m);
      dstr = dstr + '-' + ((Number(day.text) > 9) ? day.text + '' : '0' + day.text);
      this.CalendarInit(dstr);
    }
  }
  public showInquiry = false;
  public calendarBackground = 'theme2/assets/img/photo_3.jpg';
  GetSeo(){
    this.baseApi.apiPost('home/seo',{}).then(
      (res)=>{
        this.showInquiry = res['showInquiry'];
        if(res['calendarBackground']){
          this.calendarBackground = res['calendarBackground'];
       }
      },(err)=>{
    });
  }
  AddCalendar(item){
    this.jslib.SetStorage('gCalendar',item);
    gCalendar.requestAuthClick();
    
    window['addOK'] = ()=>{
      this.pop.setConfirm({
        content:'add.success',
        event:()=>{}
      });
    }
    window['addFail'] = ()=>{
      this.pop.setConfirm({
        content:'add.fail',
        event:()=>{}
      });
    }
  }
}
