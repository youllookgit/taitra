import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component'
@Component({
  selector: 'tradeShow-component',
  templateUrl: './tradeShow.component.html'
})
export class TradeShowComponent extends BaseComponent implements OnInit  {
  public Data;
  public Years = [];
  public selectYear;
  constructor(  
  ) {
    super();
    var thisYear = (new Date()).getFullYear();
    this.selectYear = thisYear;
    for(var i = thisYear + 1; i > thisYear - 10;i--){
     this.Years.push(i);
    }
  }

  ngOnInit() {
    this.Submit();
  }
  Submit(){
    this.Data = [];
    this.baseApi.apiPost('getdata/tradeshow',{year:this.selectYear}).then(
      (res)=>{
         console.log('tradeshow',res);
         this.Data = res;
      },(err)=>{

      });
  }
  optChange(){
    this.Submit();
  }

  
}
