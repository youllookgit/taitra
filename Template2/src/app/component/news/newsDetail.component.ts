import { Component, OnInit } from '@angular/core';
import { Router,NavigationEnd } from '@angular/router';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  templateUrl: './newsDetail.component.html'
})
export class NewsDetailComponent extends BaseComponent implements OnInit  {
  public subscribe;
  constructor(  
    public router: Router
  ) {
    super();
    //切頁畫面重置
    this.subscribe = this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      this.ngOnInit();
    });
  }
  Data:any;
  relation:any;
  ngOnInit() {
    var urlPatam = this.jslib.UrlParam();
    console.log('urlPatam',urlPatam);
    this.baseApi.apiPost('newsDetail',urlPatam).then(
      (res)=>{
        console.log('newsDetail',res);
        this.Data = res;
        if(res['releations'] && res['releations'].length > 0){
          this.relation = res['releations'];
        }else{
          this.relation = [];
        }
      },(err)=>{

      });
  }
  ngOnDestroy(){
    this.subscribe.unsubscribe();
  }

  
}
