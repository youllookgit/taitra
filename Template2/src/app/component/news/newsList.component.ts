import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
declare var $;

@Component({
  templateUrl: './newsList.component.html'
})
export class NewsListComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
  }
  DataSource:any;
  PageData:any;
  ngOnInit() {
    this.PageData = [];
    this.baseApi.apiPost('getdata/newsList',{}).then(
      (res)=>{
        this.DataSource = res;
      },(err)=>{

      });
  }
  SetPage(data){
    if(data){
      $("html, body").animate({
        scrollTop: 0
      }, 100);
       setTimeout(()=>{
        this.PageData = data;
      },100);
    }
  }
  
}
