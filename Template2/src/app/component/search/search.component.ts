import { Component, OnInit } from '@angular/core';
import { DomSanitizer,SafeHtml } from '@angular/platform-browser';
import { BaseComponent } from '../../share/component/base.component';
declare var $;
@Component({
  selector: 'search-component',
  templateUrl: './search.component.html'
})
export class SearchComponent extends BaseComponent implements OnInit  {

  constructor(  
    public _sanitizer: DomSanitizer
  ) {
    super();
  }
  gcsesearch: SafeHtml; 
  ngOnInit() {
  //   　<script>
  //   (function() {
  //     var cx = '010109983143769439714:gxwxy5rt3xa';
  //     var gcse = document.createElement('script');
  //     gcse.type = 'text/javascript';
  //     gcse.async = true;
  //     gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
  //         '//cse.google.com/cse.js?cx=' + cx;
  //     var s = document.getElementsByTagName('script')[0];
  //     s.parentNode.insertBefore(gcse, s);
  //   })();
  //   window.onload = function(){
  //   document.getElementById('gsc-i-id1').title = 'Search';
  // };
  // </script>
  // <gcse:search queryParameterName="para" as_sitesearch="">
  // </gcse:search>
    this.gcsesearch = this._sanitizer.bypassSecurityTrustHtml("<gcse:search queryparametername='param'></gcse:search>"); 
    var cx = '010109983143769439714:gxwxy5rt3xa';
    var gcse = document.createElement('script'); 
    gcse.type = 'text/javascript'; 
    gcse.async = true; 
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx; 
    var s = document.getElementsByTagName('script')[0]; 
    s.parentNode.insertBefore(gcse, s); 
    
  }

  
}
