import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import { DomSanitizer } from '@angular/platform-browser';
import {FormsModule,FormArray, FormBuilder, FormControl, FormGroup,Validators} from '@angular/forms';
declare var $;
@Component({
  templateUrl: './confDetail.component.html'
})
export class confDetailComponent extends BaseComponent implements OnInit  {
  public stepNo = 1;//1
  public mainForm: FormGroup;
  public CheckImg;
  public Data:any;
  public confList:any;
  public conf;
  public confid;
  public device;
  public ContryItem;
  public timeStart = [];
  public timeEnd = [];
  public contactType;
  public requiremsg = '';
  public checkcode;
  constructor(  
    public _sanitizer: DomSanitizer
  ) {
    super();
    var urlPatam = this.jslib.UrlParam();
    
    if(typeof urlPatam['id'] == 'undefined'){
      this.stepNo = 2;
    }
    this.requiremsg = this.i18n.translate('form.require');
  }
  public ySelect = [];
  public mSelect = [];
  public dSelect = [];
  ngOnInit() {    

    var urlPatam = this.jslib.UrlParam();
    this.confid = urlPatam['id'];
    this.baseApi.apiPost('getdata/conference',{}).then(
      (_res:any)=>{
         this.confList = _res;
         if(urlPatam['id']){
          this.conf = _res.find(function(_item){
            return _item.id == urlPatam['id'];
          });
          this.Submit();
         }
      },(_err)=>{
      });

      if(urlPatam['id'] && urlPatam['id'] != ''){
        this.baseApi.apiPost('getdata/confDevice',{id:urlPatam['id']}).then(
          (d_res:any)=>{
             console.log('confDevice',d_res);
             this.device = d_res;
             this.setYear();
          },(d_err)=>{
          });
      }
      this.CheckImg = '';
      this.baseApi.apiPost('getdata/checkcode?rand=' + this.jslib.getRandom(100),{}).then(
        (res)=>{
          console.log('checkcode',res);
          this.CheckImg = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + res['img']);
          this.checkcode = res['text'];
        },(err)=>{
      });

      this.baseApi.apiPost('getdata/countryList',{}).then(
        (res)=>{
          console.log('countryList',res);
          this.ContryItem = res;
          
          this.baseApi.apiPost('getdata/contactType',{}).then(
            (res)=>{
              this.contactType = res;
            },(err)=>{
          });

        },(err)=>{
      });


  }
  
  Submit(){
    var urlPatam = this.jslib.UrlParam();
    this.Data = [];
    this.baseApi.apiPost('conferencePage',{id:urlPatam['id']}).then(
      (res)=>{
         console.log('conferencePage',res);
         this.Data = res;
         this.Data['rental'] = this.Data['rental'].replace(/(?:\r\n|\r|\n)/g, '<br>');
      },(err)=>{

      });
  }
  
  public result;
  public check = true;
  subform(){
    this.check = true;
    var formdata = $('#mainForm').serializeObject();
    formdata['device'] = (formdata['device']) ? formdata['device'] : [];
    console.log('formdata',formdata);
    var keys = Object.keys(formdata);

    var _kindex = 0;
    keys.every( key => {
      _kindex = _kindex + 1;
      var thisdom = $('[name="' + key + '"]');
      console.log('_kindex',_kindex,key);

      var thisdom = $('[name="' + key + '"]');
      var errorcode = thisdom.attr('data-error');
      // var domname = thisdom.parents('.form_gorup').find('label').text();
      // domname = domname.replace(':','');
      // domname = domname.replace('*','');
      
      var Isrequire = (thisdom.attr('required') == 'required');

      if(Isrequire && formdata[key] == ''){
          this.pop.setConfirm({
            content:errorcode,
            event:()=>{
              thisdom.focus();
            }
          });
          this.check = false;
          return false;
      }else{
        //checkmail
        if(key == 'mail'){
          var ismail = this.jslib.checkEmail(formdata[key]);
          if(!ismail){
            this.check = false;
            this.pop.setConfirm({
              content:'form.emailerror',
              event:()=>{
                thisdom.focus();
              }
            });
            return false;
          }else{
            return true;
          }
        }else{
          return true;
        }
      }
    });
    //檢查驗證碼
    if(this.check){
      if(formdata['checkCodeNo'] != this.checkcode){
        this.check = false;
        this.pop.setConfirm({
          content:'form.checkValid',
          event:()=>{
            $('[name="checkCodeNo"]').focus();
          }
        });
      }
    }
    if(this.check){
      console.log('this.check',this.check);
      this.result = formdata;
      this.addStep();
    }
  }
  Reset(){
    this.isend = false;
    this.pop.setConfirm({
      content: 'form.isclear',
      cancelTxt:'form.cancel',
      event:(result)=>{
        $("#mainForm").trigger('reset')
        this.device = [];
        this.ySelect = [];
        this.setYear();
        this.mSelect = [];
        this.dSelect = [];
        this.timeStart = [];
        this.timeEnd = [];
        $('[name="contactType"]').val([1]);
      }
    });
   
  }
  public isend = false;
  formsend(){
    //reset data
    var clone = JSON.parse(JSON.stringify(this.result));
    clone['startDate'] = 
    this.result.date1 + 
    '-' + this.result.date2 + 
    '-' + this.result.date3;
    clone['hour'] = Number(this.result.endHr) - Number(this.result.since);

    if(typeof clone['device'] =='string'){
      clone['device'] = [clone['device']];
    }

    this.isend = true;
    console.log('this.check',this.check);
    //sendData
    this.baseApi.apiPostData('post/conference',clone).then(
      (send_res)=>{
        this.pop.setConfirm({
          content:'conf.ok',
          event:()=>{
            this.isend = false;
            this.stepNo = 2;
            //reset
            $("#mainForm").trigger('reset');
            this.device = [];
            this.ySelect = [];
            this.setYear();
            this.mSelect = [];
            this.dSelect = [];
            this.timeStart = [];
            this.timeEnd = [];
            $('[name="contactType"]').val([1]);

          }
        });
      },(send_err)=>{
        var msg = send_err['error'];
        if(msg){
          this.pop.setConfirm({
            content:msg,
            event:()=>{
              this.isend = false;
              this.CheckImg = '';
              this.baseApi.apiPost('getdata/checkcode?rand=' + this.jslib.getRandom(100),{}).then(
                (res)=>{
                  this.CheckImg = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + res['img']);
                  this.checkcode = res['text'];
                });
                this.stepNo = 2;
            }
          });
        }
        
      });
  }
  setYear(){
    var now = new Date();
    var y = now.getFullYear();
    this.ySelect = [y,(y+1)];
  }
  public datevalid = false;
  ychange(){
    var y = $('[name="date1"]').val();
    var now = new Date();
    if(y == now.getFullYear()){
       this.mSelect = [];
       var m = now.getMonth();
       for(var i = m;i < 12; i ++){
        this.mSelect.push(i+1);
       }
    }else{
      this.mSelect = [];
      if(y != ''){
        for(var i = 1;i <= 12; i ++){
          this.mSelect.push(i);
        }
      }
    }
    //clear
    $('[name="date2"]').val('');
    $('[name="date3"]').val('');
    $('[name="since"]').val('');
    $('[name="endHr"]').val('');
  }
  mchange(){
    var _y = $('[name="date1"]').val();
    var _m = $('[name="date2"]').val();
    var _room = $('[name="id"]').val();
    this.dSelect = [];
    this.baseApi.apiPost('getdata/confReserve',
    {
      year:_y,
      month:_m,
      room:_room
    }).then(
      (m_res:any)=>{
        console.log('m_res',m_res);
        this.dSelect = m_res[0];
      },(err)=>{

    });
    $('[name="date3"]').val('');
    $('[name="since"]').val('');
    $('[name="endHr"]').val('');
  }
  dchange(){
    var _room = $('[name="id"]').val();
    var select_m = $('[name="date2"]').val();
    var select_d = $('[name="date3"]').val();
    select_m = (select_m.length == 1) ? '0' + select_m : select_m;
    select_d = (select_d.length == 1) ? '0' + select_d : select_d;
    var _day = $('[name="date1"]').val() + '-'
    + select_m + '-'
    + select_d;

    this.baseApi.apiPost('getdata/confRangeStart',
    {
      date:_day,
      room:_room
    }).then(
      (s_res:any)=>{
        this.timeStart = s_res;
      },(err)=>{
    });
    $('[name="endHr"]').val('');
  }
  sincechange(){
    var _room = $('[name="id"]').val();
    var select_m = $('[name="date2"]').val();
    var select_d = $('[name="date3"]').val();
    select_m = (select_m.length == 1) ? '0' + select_m : select_m;
    select_d = (select_d.length == 1) ? '0' + select_d : select_d;
    var _day = $('[name="date1"]').val() + '-'
    + select_m + '-'
    + select_d;
    
    var _start = $('[name="since"]').val();
    this.baseApi.apiPost('getdata/confRangeEnd',
    {
      start:_start,
      date:_day,
      room:_room
    }).then(
      (e_res:any)=>{
        this.timeEnd = e_res;
      },(err)=>{
    });
    
  }
  daysInMonth(year,month) { 
    return new Date(Number(year), Number(month), 0).getDate(); 
  } 
  addStep(){
    this.stepNo = this.stepNo+1;
    window.scrollTo(0, 0);
  }
  lessStep(){
    this.stepNo = this.stepNo-1;
    window.scrollTo(0, 0);
  }
  confChange(){
    this.device = [];
    var _id = $('[name="id"]').val();
    this.baseApi.apiPost('getdata/confDevice',{id:_id}).then(
      (d_res:any)=>{
        this.device = d_res;
      },(d_err)=>{
      });
    this.conf = this.confList.find(function(_item){
        return _item.id == _id;
    });
    this.setYear();
  }

  //display
  CofName(id){
    var data = this.confList.find(item=>{return item.id == id});
    return (data) ? data['name'] : '';
  }
  DeviceName(ids){
    console.log('DeviceName',ids);
    var str = [];
    this.device.forEach(element => {
      if(ids.includes(element.code)){
        str.push(element.name);
      }
    });
    return str.join(',');
  }
  CtypeName(value){
    var data = this.contactType.find(item=>{return item.code == value});
    return (data) ? data['name'] : '';
  }
  CName(id){
    var data = this.ContryItem.find(item=>{return item.code == id});
    return (data) ? data['name'] : '';
  }
  refresh(){
    this.baseApi.apiPost('getdata/checkcode?rand=' + this.jslib.getRandom(100),{}).then(
      (imgres)=>{
        this.CheckImg = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + imgres['img']);
        this.checkcode = imgres['text'];
      });
  }
}
