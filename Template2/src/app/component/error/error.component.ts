import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import {I18nService} from '../../share/service/i18n.service'
@Component({
  selector: 'error-component',
  templateUrl: './error.component.html'
})
export class ErrorComponent extends BaseComponent implements OnInit  {

  constructor(  
    //public i18n : I18nService
  ) {
    super();
  }
  msg = '';
  ngOnInit() {
 
    setTimeout(()=>{
      this.msg = 'time outmsg';
    },400);
  }

  transLang(lang){
    this.i18n.setLang(lang);
  }
  
}
