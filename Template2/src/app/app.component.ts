import { Component, OnInit ,NgZone } from '@angular/core';
import { Router,RouterOutlet, ActivatedRoute, NavigationEnd } from '@angular/router';
import{ fader } from './app-routing.animations'
import { BaseComponent } from './share/component/base.component';
import {I18nService} from './share/service/i18n.service';
declare var $;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations:[
    fader
  ]
})
export class AppComponent extends BaseComponent implements OnInit  {

  // public title = "Angular Base";
  menuOpen = false;
  public langCodeSet = '';
  public IsMobile = false;
  public topStyle = {bottom:'4.5rem'};
  constructor(  
    public i18n: I18nService,
    public router: Router,
    public ngzone: NgZone
  ) {
    super(); //繼承實例化
    if(this.jslib.ClientWidth() < 768){
      this.IsMobile = true;
      //若手機版有cookie政策 需往上推
      var isCookie = this.jslib.GetStorage('cookie');
      if(typeof isCookie == 'undefined' || isCookie == true){
        this.topStyle.bottom = '11rem';
      }
    }else{
      this.IsMobile = false;
      if(typeof isCookie == 'undefined' || isCookie == true){
        this.topStyle.bottom = '10.5rem';
      }
    }
    this.GetLang().then(
      (res)=>{
       console.log('languages',res);
       this.languages = res['languages'];
      //語系處理
      var _lang = this.jslib.GetStorage('lang');;
      var _index = this.languages.findIndex(_langitem => {return _lang == _langitem.value});
      if(typeof _lang == 'undefined' || _index == -1){
        this.i18n.setLang(res['default']);
        window.location.href = window.location.href;
      }
       this.langCodeSet = this.i18n.getLangCode();
       this.GetSeo();
       this.GetMenu();
       this.GetSocialurl();
       this.GetFooter();
      },(err)=>{
    });
  }
  
  
  public menuData;//主選單
  public topmenu;//上選單
  ngOnInit() {
    //切頁畫面重置
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      console.log('events',evt.url);
      this.AddHistory(evt.url);

      window.scrollTo(0, 0);
      this.menuOpen = false;
    });
    //back event
    this.ngzone.runOutsideAngular(()=>{
        $(window).scroll(function () {
          if ($(this).scrollTop() > 100) {
              $('#top').fadeIn();
          } else {
              $('#top').fadeOut();
          }
      });
      $('#top').click(function () {
          $("html, body").animate({
              scrollTop: 0
          }, 600);
          return false;
      });
    });

    var tempW = this.jslib.ClientWidth();
    //resize reload
    $(window).resize(function() {
      var nowW = this.jslib.ClientWidth();
      if(tempW > 768 && nowW <= 768){
        window.location.reload();
      }
      if(tempW < 768 && nowW >= 768){
        window.location.reload();
      }
      //window.location.reload();
      //console.log('WDiff',tempW,nowW);
    });

    setTimeout(function(){
      //搜尋按鈕
      $('.mSearch').click(function(){
        $('#mSearch_bar').animate({left:"0",opacity:"100"});
       });
       
       $('#mSearchback').click(function(){
       $('#mSearch_bar').animate({left:"100%",opacity:"0"});
       });
    },250);
  }

  AddHistory(url){
    var list = this.jslib.GetStorage('history');
    list =  (list) ? list : [];
    list.push(url);
    //keep 10
    if(list.length > 10){
      list = list.slice(1);
    }
    this.jslib.SetStorage('history',list);
  }

  public languages;
  GetLang(){
   return this.baseApi.apiPost('home/lang',{});
  }
  public seo;
  public webtitle;
  GetSeo(){
    this.baseApi.apiPost('home/seo',{}).then(
      (res)=>{
       
       this.seo = res;
       this.setMetaTtitle(res['title']);
       var _lang = this.i18n.getLangCode();
       console.log('seo _lang',_lang);
       if(_lang == 'zh_TW'){
         this.webtitle = res['chineseTitle'];
       }else{
        this.webtitle = res['englishTitle'];
       }
       this.setMeta('description',res['description']);
       this.setMeta('keywords',res['keywords']);
      },(err)=>{
    });
  }
  public Socialurl;
  GetSocialurl(){
    this.baseApi.apiPost('home/socialurl',{}).then(
      (res)=>{
      // console.log('Socialurl',res);
       this.Socialurl = res;
       var allvalue = Object.values(res);
      },(err)=>{
    });
  }

  GetMenu(){
    this.menuData = [];
    this.baseApi.apiPost('home/menu',{}).then(
      (res)=>{
        this.menuData = this.RouteHandler(res);
      },(err)=>{
    });
  }

  RouteHandler(menuObj){
    menuObj.forEach(item => {
      item.link = this.RouteMap(item.link);
       if(item.sub && item.sub.length > 0){
        item.sub.forEach(_s => {
            _s.link = this.RouteMap(_s.link);
        });
       }
    });
    return menuObj;
  }
  //網址轉換
  RouteMap(menuUrl){
    var hosturl = window.location.protocol + '//' + window.location.host;
    if(typeof menuUrl == 'number'){
      menuUrl = '' + '/note/detail?id=' + menuUrl;
      return menuUrl;
    }else{
      menuUrl = menuUrl.replace(hosturl,'');

      menuUrl = menuUrl.replace('/news/index.jsp','/news/list');
      menuUrl = menuUrl.replace('/event/index.jsp','/event/list');
      menuUrl = menuUrl.replace('/note.jsp','/note/detail');
      menuUrl = menuUrl.replace('/about/newhandler.do','/contactUs');
      menuUrl = menuUrl.replace('/business/update.jsp','/trade');
      return menuUrl;
    }
  }

  LangChange(){
    console.log('langCodeSet',this.langCodeSet);
    if(this.langCodeSet != ''){
      this.i18n.setLang(this.langCodeSet);
      window.location.href = '/';
    }
  }
  //開關主選單
  openMenu(){
    this.menuOpen = !this.menuOpen;
   }
  //backtop
  eventHandler(code){
    if(code == 13){
      this.searchSend();
    }
  }
  searchTxt ='';
  searchSend(){
    window.location.href = '/search?param=' + this.searchTxt;
  }
  //footer
  footer;
  GetFooter(){
    this.baseApi.apiPost('home/footer',{}).then(
      (f_res)=>{
        this.footer = f_res;
      },(err)=>{
    });
  }
}
