import { Component, OnInit } from '@angular/core';
import { Router,NavigationEnd } from '@angular/router';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  templateUrl: './calendar.component.html'
})
export class CalendarComponent extends BaseComponent implements OnInit  {
 
  frameUrl:any;
  constructor(  
    public router: Router
  ) {
    super();
    var lang = this.i18n.getLangCode();
    console.log('lang',lang);
    var dom = (window.location.hostname).split('.');
    var domName = dom[0];
    domName = (domName == 'localhost') ? 'kh' : domName;
    this.frameUrl = 'https://officeportal.taiwantrade.com/calendar_embeded.jsp?lang=' + lang + '&domain='+ domName;
  }
  
  ngOnInit() {
    
  }
  ngOnDestroy(){
  }
}
