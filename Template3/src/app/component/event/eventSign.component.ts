import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BaseComponent } from '../../share/component/base.component';
import {FormsModule,FormArray, FormBuilder, FormControl, FormGroup,Validators} from '@angular/forms';
declare var $;
@Component({
  selector: 'eventSign-component',
  templateUrl: './eventSign.component.html'
})
export class eventSignComponent extends BaseComponent implements OnInit  {

  public Joins = [1,2,3,4,5];
  public requiremsg = '';
  public CheckImg;
  public FormObj;
  Data:any;
  constructor(  
    public _sanitizer: DomSanitizer
  ) {
    super();
    var urlPatam = this.jslib.UrlParam();
    this.baseApi.apiPost('eventPage',urlPatam).then(
      (res)=>{
        if(res['fee']){
          res['fee'] = res['fee'];
        }else{
          res['fee'] = '';
        }
        this.Data = res;
      },(err)=>{});

    this.baseApi.apiPost('onlineForm',urlPatam).then(
      (_res)=>{
        if(_res['fields']){
          this.FormObj = _res['fields'];
          console.log('onlineForm',this.FormObj)
        }
      },(err)=>{
      });

    this.baseApi.apiPost('getdata/countryList',{}).then(
      (res)=>{
        this.ContryItem = res;
      },(err)=>{
    });

    this.baseApi.apiPost('getdata/checkcode?rand=' + this.jslib.getRandom(100),{}).then(
      (res)=>{
        this.CheckImg = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + res['img']);
        this.checkcode = res['text'];
      },(err)=>{

      });
  }
  //public CheckImg;
  public ContryItem;
  ngOnInit() {
    var _lang = this.langCode.replace('_','-');
    $.datetimepicker.setLocale(_lang);
    this.SetDatePick();
  }
  
  Reset(){
    this.isend = false;
    this.pop.setConfirm({
      content: 'form.isclear',
      cancelTxt:'form.cancel',
      event:(result)=>{
        $("#mainForm").trigger('reset');
      }
    });
  }

  public isend = false;
  public check = true;
  public checkcode = '';
  public postData;
  formsend(){
    this.postData = null;
    this.check = true;
    var formdata = $('#mainForm').serializeObject();
    console.log('formdata',formdata);
    var keys = [];
    this.FormObj.forEach(fitem => {
      keys.push('i' + fitem.id);
    });
    keys.push('checkCodeNo');
    
    keys.every( key => {
      
      var thisdom = $('[name="' + key + '"]');
      var _k = key.replace('i','');
      var formSet = this.FormObj.find((item)=>{
        return item.id == _k;
      })
      var errorcode = '';
      var Isrequire = false;
      if(_k != 'checkCodeNo'){
        errorcode = formSet.emptyMessage;// thisdom.attr('data-error');
        Isrequire = (formSet.required == 'true');//(thisdom.attr('required') == 'required');
      }else{
        errorcode = thisdom.attr('data-error');
        Isrequire = true;
      }

      var _keyValue = formdata[key];
      if(key != 'i55'){
        _keyValue = (_keyValue) ?　_keyValue : '';
      }

      if(Isrequire && (_keyValue == '')){
          this.pop.setConfirm({
            content:errorcode,
            event:()=>{
              thisdom.focus();
            }
          });
          this.check = false;
          return false;
      }else{
        //checkmail
        if(key == 'i39'){
          var ismail = this.jslib.checkEmail(formdata[key]);
          if(!ismail){
            this.check = false;
            this.pop.setConfirm({
              content:'form.emailerror',
              event:()=>{
                thisdom.focus();
              }
            });
            return false;
          }
        }
        if(key == 'i55'){
            
            //整理參加人員
            var joinM = [];
            for(var i =1;i<=5;i++){
              var _gender = $('[name="join[' + i + '].gender"]').val();
              var _fn = $('[name="join[' + i + '].fn"]').val();
              var _ln = $('[name="join[' + i + '].ln"]').val();
              var _jobTitle = $('[name="join[' + i + '].jobTitle"]').val();
              var _email = $('[name="join[' + i + '].email"]').val();

              if(_gender !='' && _fn !=''&& _ln !=''){
                joinM.push({
                  gender:_gender,
                  fn:_fn,
                  ln:_ln,
                  jobTitle:_jobTitle,
                  email:_email
                });
              }else{
                if(_gender =='')
                  joinDom = $('[name="join[' + i + '].gender"]');
                if(_fn =='')
                  joinDom = $('[name="join[' + i + '].fn"]');
                if(_ln =='')
                  joinDom = $('[name="join[' + i + '].ln"]');
              }
              //整理欄位
              delete formdata['join[' + i + '].gender'];
              delete formdata['join[' + i + '].fn'];
              delete formdata['join[' + i + '].ln'];
              delete formdata['join[' + i + '].jobTitle'];
              delete formdata['join[' + i + '].email'];
            }
            if(joinM.length == 0){
              var joinDom;
              if($('[name="join[1].ln"]').val() =='')
                joinDom = $('[name="join[1].ln"]');
              if($('[name="join[1].fn"]').val() =='')
                joinDom = $('[name="join[1].fn"]');
              if($('[name="join[1].gender"]').val() =='')
                joinDom = $('[name="join[1].gender"]');

              this.check = false;
              this.pop.setConfirm({
                content:errorcode,
                event:()=>{
                  joinDom.focus();
                }
              });
              return false;
            }else{
              formdata[key] = joinM;
              return true;
            }
        }else{
          return true;
        }
      }
      
    });


    //檢查驗證碼
    if(this.check){
      if(formdata['checkCodeNo'] != this.checkcode){
        this.check = false;
        this.pop.setConfirm({
          content:'form.checkValid',
          event:()=>{
            $('[name="checkCodeNo"]').focus();
          }
        });
      }
    }
    if(this.check){
      //add id lang
      var _id = this.jslib.UrlParam()['id']
      var extend = {id:Number(_id),lang:this.langCode}
      var post = Object.assign(formdata,extend);
      this.postData = post;
      this.isend = true;
      window.scrollTo(0, 0);
    }
  }
  refresh(){
    this.baseApi.apiPost('getdata/checkcode?rand=' + this.jslib.getRandom(100),{}).then(
      (imgres)=>{
        this.CheckImg = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + imgres['img']);
        this.checkcode = imgres['text'];
      });
  }
  Back(){
    this.isend = false;
    window.scrollTo(0, 0);
  };
  Formpost(){
    this.baseApi.apiPostData('post/onlineForm',this.postData).then(
      (res)=>{
        this.pop.setConfirm({
          content:'conf.ok',
          event:()=>{
            this.isend = false;
            $("#mainForm").trigger('reset');
            window.scrollTo(0, 0);
          }
        });
      },(error)=>{
        this.pop.setConfirm({
          content:error['error'],
          event:()=>{
            this.isend = false;
            this.GetCheckCode();
          }
         });
       }
    )
  }
  GetCheckCode(){
    this.CheckImg = '';
    this.baseApi.apiPost('getdata/checkcode?rand=' + this.jslib.getRandom(100),{}).then(
      (cres)=>{
        this.CheckImg = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + cres['img']);
        this.checkcode = cres['text'];
      },(err)=>{
      });
  }
  GetDisPlay(key,target = ''){
    var _k = key.replace('i','');
    var formSet = this.FormObj.find((item)=>{
      return item.id == _k;
    });
    var _txt = this.postData[key];

    _txt = (typeof _txt == 'undefined') ? '' :_txt;

    if(formSet.options.length > 0){
       var optxt = formSet.options.find((_op)=>{
        return _op.value == _txt;
      });
      if(optxt)
        _txt = optxt.text;
    }

    if(formSet.type == 'COUNTRY'){
      var optxt = this.ContryItem.find((_op)=>{
        return _op.code == Number(_txt);
      });
      if(optxt)
        _txt = optxt.name;
    }

    if(formSet.type == 'JOIN_MEMBER'){
      var optxt = formSet.options.find((_op)=>{
        return _op.value == target;
      });
      if(optxt)
        _txt = optxt.text;
    }

    var html = '<span class="ans">' + _txt + '</span>';
    return html;
  }

  SetDatePick(){
    setTimeout(()=>{
      var adate = $('[name="i288"]');
      var sdate = $('[name="i289"]');
      if(adate){
        adate.datetimepicker({
          timepicker:false,
          format:'Y-m-d'
         });
      }
      if(sdate){
        sdate.datetimepicker({
          timepicker:false,
          format:'Y-m-d'
         });
      }
    },500);
  }
}
