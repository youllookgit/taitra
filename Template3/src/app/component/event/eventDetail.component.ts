import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  templateUrl: './eventDetail.component.html'
})
export class EventDetailComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
  }
  Eid;
  Data:any;
  ngOnInit() {
    var urlPatam = this.jslib.UrlParam();
    this.Eid = urlPatam['id'];
    this.baseApi.apiPost('eventPage',urlPatam).then(
      (res)=>{
        if(res['fee']){
          res['fee'] = res['fee'];
        }else{
          res['fee'] = '';
        }
        this.Data = res;
      },(err)=>{

      });
  }

  
}
