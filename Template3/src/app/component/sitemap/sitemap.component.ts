import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  selector: 'sitemap-component',
  templateUrl: './sitemap.component.html'
})
export class SitemapComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
  }
  public menuData;
  ngOnInit() {
    this.menuData = [];
    this.baseApi.apiPost('sitemap',{}).then(
      (res)=>{
        this.menuData.forEach(item => {
          if(item.sub == null){
            item['sub'] = [];
          }
        });
        this.menuData = res;
      },(err)=>{

    });
  }
}
