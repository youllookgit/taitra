import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  templateUrl: './announceDetail.component.html'
})
export class AnnounceDetailComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
  }
  Data:any;
  ngOnInit() {
    var urlPatam = this.jslib.UrlParam();
    console.log('urlPatam',urlPatam);
    this.baseApi.apiPost('announcePage',urlPatam).then(
      (res)=>{
        console.log('announcePage',res);
        this.Data = res;
      },(err)=>{

      });
  }

  
}
