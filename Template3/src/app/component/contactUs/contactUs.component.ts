import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BaseComponent } from '../../share/component/base.component';
import {FormsModule,FormArray, FormBuilder, FormControl, FormGroup,Validators} from '@angular/forms';
declare var $;
@Component({
  selector: 'contactUs-component',
  templateUrl: './contactUs.component.html'
})
export class contactUsComponent extends BaseComponent implements OnInit  {

  public requiremsg = '';
  public CheckImg;
  constructor(  
    public _sanitizer: DomSanitizer
  ) {
    super();
    this.baseApi.apiPost('getdata/countryList',{}).then(
      (res)=>{
        this.ContryItem = res;
      },(err)=>{
    });
    this.requiremsg = this.i18n.translate('form.require');
  }
  //public CheckImg;
  public ContryItem;
  ngOnInit() {
    this.baseApi.apiPost('getdata/checkcode?rand=' + this.jslib.getRandom(100),{}).then(
      (res)=>{
        this.CheckImg = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + res['img']);
        this.checkcode = res['text'];
      },(err)=>{

      });
  }
  
  Reset(){
    this.isend = false;
    this.pop.setConfirm({
      content: 'form.isclear',
      cancelTxt:'form.cancel',
      event:(result)=>{
        $("#mainForm").trigger('reset');
      }
    });
    
  }

  public isend = false;
  public check = true;
  public checkcode = '';
  formsend(){
    this.check = true;
    var formdata = $('#mainForm').serializeObject();
    var keys = Object.keys(formdata);
    keys.every( key => {
      
      var thisdom = $('[name="' + key + '"]');
      var errorcode = thisdom.attr('data-error');
      // var domname = thisdom.parents('.form_gorup').find('label').text();
      // domname = domname.replace(':','');
      // domname = domname.replace('*','');
      var Isrequire = (thisdom.attr('required') == 'required');

      if(Isrequire && formdata[key] == ''){
          this.pop.setConfirm({
            content:errorcode,
            event:()=>{
              thisdom.focus();
            }
          });
          this.check = false;
          return false;
      }else{
        //checkmail
        if(key == 'mail'){
          var ismail = this.jslib.checkEmail(formdata[key]);
          if(!ismail){
            this.check = false;
            this.pop.setConfirm({
              content:'form.emailerror',
              event:()=>{
                thisdom.focus();
              }
            });
            return false;
          }else{
            return true;
          }
        }else{
          return true;
        }
      }
    });
    //檢查驗證碼
    if(this.check){
      if(formdata['checkCodeNo'] != this.checkcode){
        this.check = false;
        this.pop.setConfirm({
          content:'form.checkValid',
          event:()=>{
            $('[name="checkCodeNo"]').focus();
          }
        });
      }
    }
    if(this.check){
      this.isend = true;
      //sendData
      this.baseApi.apiPostData('post/newhandler',formdata).then(
        (send_res)=>{
          this.pop.setConfirm({
            content:'form.msg',
            event:()=>{
              // this.Reset();
              this.isend = false;
              $("#mainForm").trigger('reset');
            }
          });
        },(send_err)=>{
          var msg = send_err['error'];
          if(msg){
            this.pop.setConfirm({
              content:msg,
              event:()=>{
                this.isend = false;
                this.baseApi.apiPost('getdata/checkcode?rand=' + this.jslib.getRandom(100),{}).then(
                  (res)=>{
                    this.CheckImg = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + res['img']);
                    this.checkcode = res['text'];
                  });
              }
            });
          }
          
        });
    }
  }
  refresh(){
    this.baseApi.apiPost('getdata/checkcode?rand=' + this.jslib.getRandom(100),{}).then(
      (imgres)=>{
        this.CheckImg = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + imgres['img']);
        this.checkcode = imgres['text'];
      });
  }
}
