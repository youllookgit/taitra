import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  selector: 'aboutTrade-component',
  templateUrl: './aboutTrade.component.html'
})
export class AboutTradeComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
  }
  public Data;
  ngOnInit() {
    this.baseApi.apiPost('getdata/taiwanTrade',{}).then(
      (res)=>{
        this.Data = res;
      },(err)=>{
    });
  }

  
}
