import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  selector: 'aboutus-component',
  templateUrl: './aboutUs.component.html'
})
export class AboutUsComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
  }
  
  public Manager;
  public Contacts;
  public cmap;
  ngOnInit() {
    this.baseApi.apiPost('getdata/aboutus',{}).then(
      (res)=>{
        console.log('contactUs',res);
        this.Manager = res['manager'];
        this.Contacts = res['contacts'];
        this.cmap = res['map'];
      },(err)=>{

      });
  }
  
}
