// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
//Pipe
import { I18nPipe } from './i18n.pipe';
import {NumberPipe} from './Number.pipe';
import {HtmlHandlerPipe} from './HtmlHandler.pipe';
import { SafePipe } from './Safe.pipe';
@NgModule({
  imports: [
  ],
  declarations: [
    I18nPipe,
    NumberPipe,
    HtmlHandlerPipe,
    SafePipe
  ],
  providers: [
  ],
  exports: [I18nPipe,NumberPipe,HtmlHandlerPipe,SafePipe]
})
export class PipeModule { }
