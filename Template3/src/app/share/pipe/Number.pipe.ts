import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'caseNo'
})
export class NumberPipe implements PipeTransform {

  transform(value: any): any {
      if(value){
        var _length = 5 - value.toString().length;
        var add = '';
        for(var i=0;i<_length;i++){
          add += '0';
        }
        return add + value.toString();
      }
      else{
        return value;
      }
    
  }
}
