import { Directive, ElementRef, HostListener, Input, Inject } from '@angular/core';
import { Router} from '@angular/router';
declare var jslib;
@Directive(
    {
         selector: '[urlHref]',
         host : {
            '(click)' : 'preventDefault($event)'
          }
    }
)
export class UrlHrefDirective {
    // @Input() href;
    constructor(
        public el: ElementRef,
        public router: Router
    ) {
    }
    preventDefault(event) {
        console.log('preventDefault',event);
        event.preventDefault();
        event.stopPropagation();
        var url = this.el.nativeElement['href'];
        var baseUrl = window.location.origin;
        url = url.replace(baseUrl,'');
        console.log('event',event);
        if(url.indexOf('http') > -1 || url.indexOf('www') > -1){
            window.open(url, '_blank');
        }
        else{
            var _url = url.replace(window.location.origin,'');
            _url = encodeURI(_url);
            console.log(_url);
            if(_url != 'null' || _url != '/null'){
                window.location.href = _url;
            }
            //this.router.navigateByUrl(_url);
        }
    }
}