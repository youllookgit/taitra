import { Component } from '@angular/core';
import { LoadingService } from '../service/loading.service'

@Component({
  selector: 'app-pop-confirm',
  styleUrls: ['./pop-confirm.css'],
  templateUrl: './pop-confirm.component.html'
})
export class PopConfirmComponent {
  public _subscribe;
  
  public open = false;
  public title = '';
  public content = '';
  public cancelTxt = '';
  public event: any;

  constructor(
    public pop: LoadingService
  ) {
    console.log('PopConfirmComponent constructor');

    this._subscribe = this.pop.confirnSetting.subscribe(
      (setting: any) => {
        this.open = true;
        if(typeof setting == 'object'){
          this.title = setting['title'] || '';
          this.content = setting['content'] || '';
          this.cancelTxt = setting['cancelTxt'] || '';
          this.event = setting['event'];
        }
        if(typeof setting == 'string'){
          this.title = setting;
        }
       
      }
    );
  }

  Cancel() {
    this.open = false;
  }

  Submit() {
    if (typeof this.event == 'function') {
      this.event(true);
    }
    this.open = false;
  }
  ngOnDestroy() {
    this._subscribe.unsubscribe();
  }
}




