import { Component, OnInit,OnChanges,Input,Output,EventEmitter,NgZone } from '@angular/core';
import { Router} from '@angular/router';
declare var Swiper;
declare var jslib;
@Component({
  selector: 'homeBnr',
  templateUrl: './homebnr.component.html'
})
export class HomeBnrComponent implements OnInit//OnChanges 
{
  @Input() SourceData;
  @Output() PageData = new EventEmitter();
  public IsMobile = false;
  public bnrStyle = {width:'100vw'};
  public showInquiry = true;
  
  constructor(
    public ngzone : NgZone,
    public router: Router
  ) {
    if(jslib.ClientWidth() < 768 ){
      this.IsMobile = true;
    }else{
      this.IsMobile = false;
    }
    this.showInquiry = jslib.GetStorage('showInquiry');
   console.log('HomeBnrComponent ngOnInit',this.showInquiry);
  }
  ngOnInit() {
   // console.log('HomeBnrComponent ngOnInit');
  }
  public swiper;
  ngOnChanges() {
   this.swiper = null;
   setTimeout(()=>{
      if(this.swiper == null){
        this.swiper = new Swiper('.homebanner', {
          autoHeight: true,
          pagination: {
            el: '.swiper-pagination',
          },
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          }
        });
      }
      this.showInquiry = jslib.GetStorage('showInquiry');
    },1000);

    setTimeout(()=>{
      if(jslib.ClientWidth() < 768 ){
        this.IsMobile = true;
      }else{
        this.IsMobile = false;
      }
    },1100);
    
  }
  BannerClick(){
    var index = this.swiper.realIndex;
    var url = this.SourceData[index]
    if(url['url'] != null){
      window.open(url['url'], '_blank');
    };
  }
  ToTrade(){
    this.router.navigate(['/trade']);;
  }
  
  
}
