import { Component, OnInit } from '@angular/core';
import { LoadingService } from '../service/loading.service';
@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html'
})
export class LoadingComponent implements OnInit {
  public Isopen = false;
  public className = 'loadingFadeOut';
  constructor(private loadservice : LoadingService) { }

  ngOnInit() {
    this.loadservice.eventUpdated.subscribe(
      () => {
        this.Isopen = this.loadservice.getLoading();
        if(this.loadservice.getLoading()){
          //this.className = 'loadingFadeIn';
        }else{
          //this.className = 'loadingFadeOut';
        }
      }
    );
  }
}
