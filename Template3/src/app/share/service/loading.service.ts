import { Injectable, ViewChild ,EventEmitter } from '@angular/core';

@Injectable()
export class LoadingService {
  public eventUpdated:EventEmitter<boolean> = new EventEmitter();
  public confirnSetting:EventEmitter<object> = new EventEmitter();
  public open = false;
  constructor() { 
    
  }
  
  setLoading(Isopen:boolean) {
    this.open = Isopen;
    //console.log('LoadingService setLoading',Isopen);
    this.eventUpdated.emit(this.open);
  }

  getLoading(){
   // console.log('LoadingService getLoading');
    return this.open;
  }

  setConfirm(Setting:any) {
    if(typeof Setting == 'object'){
      this.confirnSetting.emit(Setting);
    }else if(typeof Setting == 'string'){
      this.confirnSetting.emit({content:Setting});
    }else{
      this.confirnSetting.emit(Setting);
    }
  }

}
