import { Injectable, ViewChild ,EventEmitter } from '@angular/core';

//langs
import { Config } from '../../../theme3/assets/configuration/config';

import {AR_SA_TRANS} from '../../../theme3/assets/configuration/lang/ar_SA';
import {BG_BG_TRANS} from '../../../theme3/assets/configuration/lang/bg_BG';
import {BN_BD_TRANS} from '../../../theme3/assets/configuration/lang/bn_BD';
import {DE_DE_TRANS} from '../../../theme3/assets/configuration/lang/de_DE';
import {EN_US_TRANS} from '../../../theme3/assets/configuration/lang/en_US';
import {ES_ES_TRANS} from '../../../theme3/assets/configuration/lang/es_ES';
import {FA_IR_TRANS} from '../../../theme3/assets/configuration/lang/fa_IR';
import {FR_FR_TRANS} from '../../../theme3/assets/configuration/lang/fr_FR';
import {HU_HU_TRANS} from '../../../theme3/assets/configuration/lang/hu_HU';
import {IN_ID_TRANS} from '../../../theme3/assets/configuration/lang/in_ID';
import {IT_IT_TRANS} from '../../../theme3/assets/configuration/lang/it_IT';
import {JA_JP_TRANS} from '../../../theme3/assets/configuration/lang/ja_JP';
import {KO_KR_TRANS} from '../../../theme3/assets/configuration/lang/ko_KR';
import {MN_MN_TRANS} from '../../../theme3/assets/configuration/lang/mn_MN';
import {MS_MY_TRANS} from '../../../theme3/assets/configuration/lang/ms_MY';
import {NL_NL_TRANS} from '../../../theme3/assets/configuration/lang/nl_NL';
import {PL_PL_TRANS} from '../../../theme3/assets/configuration/lang/pl_PL';
import {PT_PT_TRANS} from '../../../theme3/assets/configuration/lang/pt_PT';
import {RO_RO_TRANS} from '../../../theme3/assets/configuration/lang/ro_RO';
import {RU_RU_TRANS} from '../../../theme3/assets/configuration/lang/ru_RU';
import {TH_TH_TRANS} from '../../../theme3/assets/configuration/lang/th_TH';
import {TR_TR_TRANS} from '../../../theme3/assets/configuration/lang/tr_TR';
import {UK_UA_TRANS} from '../../../theme3/assets/configuration/lang/uk_UA';
import {VI_VN_TRANS} from '../../../theme3/assets/configuration/lang/vi_VN';
import {ZH_TW_TRANS} from '../../../theme3/assets/configuration/lang/zh_TW';
import {ZH_CN_TRANS} from '../../../theme3/assets/configuration/lang/zh_CN';


declare var jslib:any; //呼叫第三方js

@Injectable()
export class I18nService {
  public eventUpdated:EventEmitter<boolean> = new EventEmitter();
  public Langs:any;
  public nowLang;
  constructor() {
    this.Langs = {
      'zh_TW' : ZH_TW_TRANS,
      'en_US' : EN_US_TRANS,
      'zh_CN' : ZH_CN_TRANS,
      'ja_JP' : JA_JP_TRANS,
      'de_DE' : DE_DE_TRANS,
      'fr_FR' : FR_FR_TRANS,
      'es_ES' : ES_ES_TRANS,
      'pt_PT' : PT_PT_TRANS,
      'ru_RU' : RU_RU_TRANS,
      'it_IT' : IT_IT_TRANS,
      'ko_KR' : KO_KR_TRANS,
      'pl_PL' : PL_PL_TRANS,
      'ar_SA' : AR_SA_TRANS,
      'vi_VN' : VI_VN_TRANS,
      'mn_MN' : MN_MN_TRANS,
      'tr_TR' : TR_TR_TRANS,
      'bn_BD' : BN_BD_TRANS,
      'fa_IR' : FA_IR_TRANS,
      'in_ID' : IN_ID_TRANS,
      'bg_BG' : BG_BG_TRANS,
      'uk_UA' : UK_UA_TRANS,
      'nl_NL' : NL_NL_TRANS,
      'ms_MY' : MS_MY_TRANS,
      'th_TH' : TH_TH_TRANS,
      'hu_HU' : HU_HU_TRANS,
      'ro_RO' : RO_RO_TRANS
    };
    //Default
    var langcode = jslib.UrlParam()['lang'];
    if(this.Langs[langcode]){
      jslib.SetStorage('lang',langcode);
      this.nowLang = this.Langs[langcode];
    }else{
      var _lang = jslib.GetStorage('lang');
      _lang = (_lang) ? _lang : Config.DefaultLang;
      this.nowLang = this.Langs[_lang];
    }
  }
  
  setLang(langCode) {
    if(this.Langs[langCode]){
      jslib.SetStorage('lang',langCode);
      this.nowLang = this.Langs[langCode];
      console.log('nowLang',this.nowLang);
      this.eventUpdated.emit(langCode);
    }
  }

  getLang(){
    if(this.nowLang){
      return this.nowLang;
    }else{
      var _lang = jslib.GetStorage('lang');
      _lang = (_lang) ? _lang : Config.DefaultLang;
      this.nowLang = this.Langs[_lang];
      return this.nowLang;
    }
  }
  getLangCode(){
    var _lang = jslib.GetStorage('lang');
    _lang = (_lang) ? _lang : Config.DefaultLang;
    return _lang;
  }

  translate(key){
    var nowLang = this.getLang();
    var keys = key.split('.');
    if(keys.length > 1){
      var result = nowLang[keys[0]][keys[1]];
      if(result){
        return result;
      }else{
        return key;
      }
    }else{
      var result = nowLang[key];
      if(result){
        return result;
      }else{
        return key;
      }
    }
  }
}
