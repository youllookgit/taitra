import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
declare var $;

@Component({
  selector: 'tradeMission-component',
  templateUrl: './tradeMission.component.html'
})
export class TradeMissionComponent extends BaseComponent implements OnInit  {
  public Data;
  public Years = [];
  public selectYear;
  constructor(  
  ) {
    super();
    var thisYear = (new Date()).getFullYear();
    this.selectYear = thisYear;
    for(var i = thisYear + 1; i > thisYear - 10;i--){
     this.Years.push(i);
    }
  }
  DataSource:any;
  PageData:any;
  ngOnInit() {
    this.Submit();
  }
  Submit(){
    this.baseApi.apiPost('getdata/trademission',{year:this.selectYear}).then(
      (res)=>{
         this.DataSource = res;
      },(err)=>{
      });
  }
  optChange(){
    this.Submit();
  }
  SetPage(data){
    if(data){
      $("html, body").animate({
        scrollTop: 0
      }, 100);
       setTimeout(()=>{
        this.PageData = data;
      },100);
    }
  }
  
}
