import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  templateUrl: './conference.component.html'
})
export class conferenceComponent extends BaseComponent implements OnInit  {
  public Data;
  constructor(  
  ) {
    super();
  }

  ngOnInit() {
    this.Submit();
  }
  Submit(){
    this.Data = [];
    this.baseApi.apiPost('getdata/conference',{}).then(
      (res)=>{
         console.log('conference',res);
         this.Data = res;
      },(err)=>{

      });
  }

  
}
