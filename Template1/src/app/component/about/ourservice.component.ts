import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  selector: 'ourservice-component',
  templateUrl: './ourservice.component.html'
})
export class OurServiceComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
  }
  public Data;
  ngOnInit() {
    this.baseApi.apiPost('getdata/ourService',{}).then(
      (res)=>{
         this.Data = res;
      },(err)=>{
      });
  }

  
}
