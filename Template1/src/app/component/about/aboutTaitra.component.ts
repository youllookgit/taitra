import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  selector: 'aboutTaitra-component',
  templateUrl: './aboutTaitra.component.html'
})
export class AboutTaitraComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
  }
  public Data;
  ngOnInit() {
    this.baseApi.apiPost('getdata/aboutTaitra',{}).then(
      (res)=>{
         this.Data = res;
      },(err)=>{
      });
  }

  
}
