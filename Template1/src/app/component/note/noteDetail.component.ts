import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  templateUrl: './noteDetail.component.html'
})
export class NoteDetailComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
  }
  Data:any;
  ngOnInit() {
    var urlPatam = this.jslib.UrlParam();
    console.log('urlPatam',urlPatam);
    this.baseApi.apiPost('notePage',urlPatam).then(
      (res)=>{
        console.log('notePage',res);
        this.Data = res;
      },(err)=>{

      });
  }

  
}
