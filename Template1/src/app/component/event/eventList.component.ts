import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { BaseComponent } from '../../share/component/base.component';
declare var $;

@Component({
  templateUrl: './eventList.component.html'
})
export class EventListComponent extends BaseComponent implements OnInit  {

  constructor(
    public router: Router
  ) {
    super();
  }
  DataSource:any;
  PageData:any;
  ngOnInit() {
    this.baseApi.apiPost('getdata/eventList',{}).then(
      (res:any)=>{
        console.log('res',res);
        res.forEach(_item => {
          if(_item['url'] == null || _item['url'] == ''){
            _item['url'] = '/event/detail?id=' + _item['id'];
          }
        });
        if(!res || res.length == 0){
          //若從home頁來又沒資料才轉向
          var his = this.jslib.GetStorage('history');
          var last = his[his.length-2];
          
          if((last != '/tradeShow' && last != '/tradeMission')){
            this.router.navigate(['/tradeShow']);
          }
        }
        this.DataSource = res;
      },(err)=>{

      });
  }

  SetPage(data){
    //console.log('EventList SetPage',data);
    if(data){
      $("html, body").animate({
        scrollTop: 0
      }, 100);
       setTimeout(()=>{
        this.PageData = data;
      },100);
    }
  }
  
}
