import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component'
@Component({
  selector: 'announce-component',
  templateUrl: './announce.component.html'
})
export class AnnounceComponent extends BaseComponent implements OnInit  {
  public Data;
  constructor(  
  ) {
    super();
  }

  ngOnInit() {
    this.Submit();
  }
  Submit(){
    this.Data = [];
    this.baseApi.apiPost('getdata/announceList',{}).then(
      (res)=>{
         console.log('announce',res);
         this.Data = res;
      },(err)=>{

      });
  }
  optChange(){
    this.Submit();
  }

  
}
