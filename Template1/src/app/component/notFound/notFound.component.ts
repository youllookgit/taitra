import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import {LoadingService} from '../../share/service/loading.service'
@Component({
  selector: 'notFound-component',
  templateUrl: './notFound.component.html'
})
export class NotFoundComponent extends BaseComponent implements OnInit  {

  constructor(  
    public ls : LoadingService
  ) {
    super();
    console.log('NotFoundComponent constructor');
  }

  ngOnInit() {
    console.log('NotFoundComponent ngOnInit');
    //this.ls.setLoading(true);
    // setTimeout(()=>{
    //   this.ls.setLoading(false);
    // },3000);
    this.baseApi.apiPost('eventPage',{}).then(
      (res)=>{

      },(err)=>{

      });
  }

  
}
