import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BaseComponent } from '../../share/component/base.component';
declare var $;
@Component({
  selector: 'trade-component',
  templateUrl: './trade.component.html'
})
export class TradeComponent extends BaseComponent implements OnInit  {
  
  public requiremsg = '';
  constructor(  
    public _sanitizer: DomSanitizer
  ) {
    super();
  }
  public CheckImg;
  public ContryItem;
  public btyleItem;
  public checkcode;
  ngOnInit() {
    
    this.baseApi.apiPost('getdata/countryList',{}).then(
      (res)=>{
        this.ContryItem = res;
      },(err)=>{
    });
    this.baseApi.apiPost('getdata/businessTypeSet',{}).then(
      (type_res)=>{
        this.btyleItem = type_res;
      },(type_err)=>{
    });
    this.requiremsg = this.i18n.translate('form.require');
    this.GetCheckCode();

  }

  GetCheckCode(){
    this.CheckImg = '';
    this.baseApi.apiPost('getdata/checkcode?rand=' + this.jslib.getRandom(100),{}).then(
      (cres)=>{
        this.CheckImg = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + cres['img']);
        this.checkcode = cres['text'];
      },(err)=>{
      });
  }
  Reset(){
    this.isend = false;
    this.pop.setConfirm({
      content: 'form.isclear',
      cancelTxt:'form.cancel',
      event:(result)=>{
        $("#mainForm").trigger('reset')
      }
    });
  }
  public result;
  public isend = false;
  public check = true;
  formsend(){

    this.check = true;
    var formdata = $('#mainForm').serializeObject();
    //add key
    formdata = Object.assign({type:''},formdata);
    if(!formdata['businessTypeSet']){
      formdata['businessTypeSet'] = '';
    }
    var keys = Object.keys(formdata);
    console.log('key',keys,formdata);
    var _kindex = 0;
    keys.every( key => {
      _kindex = _kindex + 1;
      console.log('_kindex',_kindex,key);
      
      var thisdom = $('[name="' + key + '"]');
      var errorcode = thisdom.attr('data-error');
      // var domname = thisdom.parents('.form_gorup').find('label').eq(0).text();
      // domname = domname.replace(':','');
      // domname = domname.replace('*','');
      
      var Isrequire = (thisdom.attr('required') == 'required');

      //順序例外處理 1
      // if(_kindex == 17){
      //   domname = thisdom.parents('td').find('label').eq(0).text();
      // }

      if(Isrequire && formdata[key] == ''){
        //順序例外處理 交易型態 2
        if(_kindex == 16 && key == 'prod[0].pname'){
          errorcode = $('[name="businessTypeSet"]').attr('data-error');
          // domname = $('[name="businessTypeSet"]').parents('.form_gorup').find('label').eq(0).text();
          // domname = domname.replace(':','');
          // domname = domname.replace('*','');
          this.pop.setConfirm({
            content:errorcode,
            event:()=>{
              $('[name="businessTypeSet"]').focus();
            }
          });
          this.check = false;
          return false;
        }else{
          this.pop.setConfirm({
            content:errorcode,
            event:()=>{
              thisdom.focus();
            }
          });
          this.check = false;
          return false;
        }

      }else{
        //checkmail
        if(key == 'mail'){
          var ismail = this.jslib.checkEmail(formdata[key]);
          if(!ismail){
            this.check = false;
            this.pop.setConfirm({
              content:'form.emailerror',
              event:()=>{
                thisdom.focus();
              }
            });
            return false;
          }else{
            return true;
          }
        }else{
          return true;
        }

      }
    });

    //檢查驗證碼
    if(this.check){
      if(formdata['checkCodeNo'] != this.checkcode){
        this.check = false;
        this.pop.setConfirm({
          content:'form.checkValid',
          event:()=>{
            $('[name="checkCodeNo"]').focus();
          }
        });
      }
    }
    //checkproduct
    if(this.check){
      var product = [];
      for(var i =0;i<3;i++){
        var _name = $('[name="prod[' + i + '].pname"]').val();
        var _desc = $('[name="prod[' + i + '].desc"]').val();
        var _purchase = $('[name="prod[' + i + '].purchase"]').val();
        if(_name!=''){
          product.push({
            pname:_name,
            desc:_desc,
            purchase:_purchase
          });
        }
        //整理欄位
        delete formdata['prod[' + i + '].pname'];
        delete formdata['prod[' + i + '].desc'];
        delete formdata['prod[' + i + '].purchase'];
      }
      if(product.length == 0){
        this.pop.setConfirm({
          content:this.i18n.translate('form.pname') + this.requiremsg,
          event:()=>{
            $('[name="prod[0].pname"]').focus();
          }
        });
        this.check = false;
        return false;
      }else{
        formdata['product'] = product;
      }
    }
    //整理電話欄位
    // if(this.check){
    //   formdata['phone'] = formdata['phone1'] + formdata['phone2'] + formdata['phone3'];
    //     if(formdata['phone4']){
    //       formdata['phone'] =formdata['phone'] + '#' + formdata['phone4'];
    //     }
    //     delete formdata['phone1'];
    //     delete formdata['phone2']; 
    //     delete formdata['phone3']; 
    //     delete formdata['phone4']; 
    // }
    if(this.check)
    {
      if(this.tempFile){
        //uploadfile
        this.baseApi.apiPostFile('upload',this.tempFile).then(
          (res)=>{
            if(res['success']){
              this.tempfileinfo = res['data'];
              formdata['file'] = this.tempfileinfo['id'];
              this.result = formdata;
              this.isend = true;
              window.scrollTo(0, 0);
            }else{
              this.pop.setConfirm({
                content:res['error']
               })
             }
          },
          (err)=>{
          },
        )
      }else{
        formdata['file'] = '';
        this.result = formdata;
        this.isend = true;
        window.scrollTo(0, 0);
      }
    }
  }
  Back(){
    this.isend = false;
    window.scrollTo(0, 0);
  };
  Formpost(){
    //businessTypeSet 防呆
    if(typeof this.result['businessTypeSet'] == 'string'){
      this.result['businessTypeSet'] = [this.result['businessTypeSet']];
    }
    this.baseApi.apiPostData('post/business',this.result).then(
      (res)=>{
        this.pop.setConfirm({
          content:'conf.ok',
          event:()=>{
            this.isend = false;
            $("#mainForm").trigger('reset');
            //this.Reset();
          }
        });
      },(error)=>{
        this.pop.setConfirm({
          content:error['error'],
          event:()=>{
            this.isend = false;
            this.GetCheckCode();
          }
         });
       }
    )
  }
  CtypeName(ids){
    var str = [];
    this.btyleItem.forEach(element => {
      if(ids.includes(element.code)){
        str.push(element.name);
      }
    });
    return str.join(',');
  }
  CName(id){
    var data = this.ContryItem.find(item=>{return item.code == id});
    return (data) ? data['name'] : '';
  }

  tempFile = null;
  tempfileinfo;
  handleFileInput(file:File){
    //uploadfile
    this.tempFile = file[0];
  }
  refresh(){
    this.baseApi.apiPost('getdata/checkcode?rand=' + this.jslib.getRandom(100),{}).then(
      (imgres)=>{
        this.CheckImg = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + imgres['img']);
        this.checkcode = imgres['text'];
      });
  }
}
