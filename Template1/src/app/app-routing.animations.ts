import {
  trigger,
  transition,
  style,
  query,
  group,
  animateChild,
  animate,
  keyframes
} from '@angular/animations';
//教學網址:https://www.youtube.com/watch?v=7JA90VI9fAI
export const fader = 
trigger('routeAnimations',[
    transition('*<=>*',[
      query(':enter,:leave',[
        style({
          position:'relative',
          width:'100vw',
          opacity:0,
          transform:'scale(1)',
        }),
      ], {optional: true}),
      query(':enter',[
        animate('600ms ease',
        style({
          opacity:1,
          transform:'scale(1)',
          position:'relative'
        })
      ),
      ], {optional: true}),
    ])
]);
