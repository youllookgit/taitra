import { Pipe, PipeTransform} from '@angular/core';
import {I18nService} from '../service/i18n.service'
@Pipe({
  name: 'i18n'
})
export class I18nPipe implements PipeTransform {

  constructor(  
    public i18n : I18nService
  ) {
    //console.log('I18nPipe constructor');
  }
  transform(key: any): any {
    var nowLang = this.i18n.getLang();
    var keys = key.split('.');
    if(keys.length > 1 && keys.length <= 2){
      if(keys[0] != '' && keys[1] != '' ){
        var result = '';
        if(nowLang[keys[0]]){
         result = nowLang[keys[0]][keys[1]];
        }else{
          result = keys.join('.');
        }
        if(result){
          return result;
        }else{
          return key;
        }
      }else{
        return key;
      }
    }else{
      if(key != ''){
        var _result = nowLang[key];
        if(_result){
          return _result;
        }else{
          return key;
        }
      }else{
        return key;
      }
      
    }

  }
}
