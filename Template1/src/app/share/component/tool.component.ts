import { Component, OnInit } from '@angular/core';
declare var $;
@Component({
  selector: 'app-tool',
  templateUrl: './tool.component.html'
})
export class ToolComponent implements OnInit {
  public Isopen = false;
  constructor() { }

  ngOnInit() {
  }
  less(){
    $('.main').css('font-size','80%');
  }
  normal(){
    $('.main').css('font-size','100%');
  }
  add(){
    $('.main').css('font-size','120%');
  }
  print(){
    window.print()
  }
}
