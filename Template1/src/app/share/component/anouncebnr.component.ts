import { Component, OnInit,OnChanges,Input,Output,EventEmitter,NgZone } from '@angular/core';
declare var Swiper;
declare var jslib;
@Component({
  selector: 'annouceBnr',
  templateUrl: './anouncebnr.component.html'
})
export class AnnouceBnrComponent implements OnInit//OnChanges 
{
  @Input() SourceData;
  @Input() ClassName;
  @Output() PageData = new EventEmitter();
  constructor(
    public ngzone : NgZone
  ) {
    
  }
  public _class;
  public swiper;
  ngOnInit() {
   
  }
  ngOnChanges() {
    this._class = this.ClassName;
    var count = 4;
    count = (jslib.ClientWidth() < 768) ? 1 : 4;
    var space = (count)*5;
    this.ngzone.runOutsideAngular(()=>{
      setTimeout(()=>{
        this.swiper = new Swiper('.' + this._class, {
          // agination: {
          //   el: '.swiper-pagination2',
          // },
         // autoplay: {delay:3000,disableOnInteraction: false}, //autopaly
          slidesPerGroup: count,
          slidesPerView: count,
          spaceBetween: space,
          navigation: {
            nextEl: '.' + this._class + '-next' ,
            prevEl: '.' + this._class + '-prev' ,
          }
        });
      },250);
    });
    
  }
}
