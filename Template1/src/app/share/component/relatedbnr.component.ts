import { Component,OnInit,OnChanges,Input,NgZone  } from '@angular/core';
declare var Swiper;
@Component({
  selector: 'relatedBnr',
  templateUrl: './relatedbnr.component.html'
})
export class RelatedBnrComponent implements OnChanges 
{
  @Input() ClassName;
  @Input() SourceData;
  constructor(
    public ngzone : NgZone
  ) {
    
  }
  public swiper;
  ngOnChanges() {
    
    setTimeout(()=>{
      this.swiper = new Swiper('.' + this.ClassName,
       {
         spaceBetween: 5,
         autoplay: {delay:3000,disableOnInteraction: false}
        });
    },250);
  }
}
