import { Component, OnInit,OnChanges,Input,Output,EventEmitter,NgZone } from '@angular/core';
declare var Swiper;
declare var jslib;
@Component({
  selector: 'homeBnr',
  templateUrl: './homebnr.component.html'
})
export class HomeBnrComponent implements OnInit//OnChanges 
{
  @Input() SourceData;
  @Output() PageData = new EventEmitter();
  public bnrStyle = {width:'100vw'};
  constructor(
    public ngzone : NgZone
  ) {
    // if(jslib.ClientWidth() < 768 ){
    //   this.bnrStyle = {width:'100vw',height:'auto'};
    // }else{
    //   this.bnrStyle = {width:'100vw',height:'auto'};
    // }
  }
  ngOnInit() {
   // console.log('HomeBnrComponent ngOnInit');
  }
  public swiper;
  ngOnChanges() {
    this.swiper = null;
    setTimeout(()=>{
       if(this.swiper == null){
         this.swiper = new Swiper('.homebanner', {
           autoHeight: true,
           pagination: {
             el: '.swiper-pagination',
           },
           navigation: {
             nextEl: '.swiper-button-next',
             prevEl: '.swiper-button-prev',
           }
         });
       }
     },1000);
   }
  BannerClick(){
    console.log('BannerClick');
    var index = this.swiper.realIndex;
    var url = this.SourceData[index]
    if(url['url'] != null){
      window.open(url['url'], '_blank');
    };
  }

  
}
