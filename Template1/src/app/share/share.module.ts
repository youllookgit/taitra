// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ApiService } from './service/api.service';
import { LoadingService } from './service/loading.service';

//import { PopService } from './service/pop.service';

// Modal Component
//import { PopComponent } from './pop/pop.component'
import { I18nService } from './service/i18n.service';
//Component
import {PaginationComponent} from './component/pagination.component';
import {HomeBnrComponent} from './component/homebnr.component';
import {AnnouceBnrComponent} from './component/anouncebnr.component';
import {CookieBoxComponent} from './component/cookiebox.component';
import {RelatedBnrComponent} from './component/relatedbnr.component';
import {ToolComponent} from './component/tool.component';
import {GmapComponent} from './component/gmap.component';
// Directuve
import { UrlHrefDirective } from '../share/directive/hrefDirective';
import { PipeModule } from '../../app/share/pipe/pipe.module';
@NgModule({
  imports: [
    CommonModule,
    //ModalModule,
    FormsModule,
    PipeModule
  ],
  declarations: [
    PaginationComponent,
    HomeBnrComponent,
    ToolComponent,
    AnnouceBnrComponent,
    GmapComponent,
    CookieBoxComponent,
    RelatedBnrComponent,
    UrlHrefDirective
  ],
  providers: [
    ApiService,
    LoadingService,
    I18nService,
  ],
  exports: [
    PaginationComponent,
    HomeBnrComponent,
    ToolComponent,
    AnnouceBnrComponent,
    CookieBoxComponent,
    GmapComponent,
    RelatedBnrComponent,
    UrlHrefDirective,
    PipeModule
    //PopTransQueryComponent
  ]
})
export class ShareModule { }
