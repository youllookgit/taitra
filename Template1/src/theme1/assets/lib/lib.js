var jslib = {};
//Url Param
jslib.UrlParam = function(){
    var s = window.location.search;
    var r = {};
    if(s){
       s = s.slice(1,s.length);//remove first '?'
       var a = s.split('&');
       
       for(var i = 0;i < a.length; i ++){
         var _a = a[i].split('=');
         r[(_a[0].toLocaleLowerCase())] = _a[1];
       }
    }
    return r;
}

jslib.ObjToParam = function(object){
    var param = '?';
    //console.log('ObjToParam');
    if(typeof object == 'object'){
        var keys = Object.keys(object);
        for(var i = 0 ; i < keys.length ;i++){
            if(i > 0){
                param = param + '&' + keys[i] + '=' + object[keys[i]];
            }else{
                param = param + keys[i] + '=' + object[keys[i]];
            }
        }
    }
    return param;
}
//LocalStorage
jslib.SetStorage = function(key,value){
    if(typeof value != 'undefined'){
        var data = localStorage.getItem('sdata');
        if(data == null){
            data = {};
            data[key] = value;
        }else{
            data = JSON.parse(data);
            data[key] = value;
        }
        localStorage.setItem('sdata',JSON.stringify(data));
    }
};
jslib.GetStorage = function(key){
    if(typeof key != 'undefined'){
        var data = localStorage.getItem('sdata');
        data = (data == null) ? {} : JSON.parse(data);
        return data[key];
    }else{
        var data = localStorage.getItem('sdata');
        data = (data == null) ? {} :  JSON.parse(data);
        return data;
    }
};
jslib.StorageClear = function(){
   localStorage.clear();
};

jslib.replaceAll = function (target,search,replacement) {
    return target.replace(new RegExp(search, 'g'), replacement);
};

jslib.ClientWidth = function () {
    return document.body.offsetWidth;
};
jslib.ClientHeight = function () {
    return document.body.offsetHeight;
};
jslib.checkEmail = function (email){
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};
jslib.getRandom= function (x){
    return Math.floor(Math.random()*x)+1;
};
  