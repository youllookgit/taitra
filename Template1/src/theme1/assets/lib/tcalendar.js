var tcalendar = {};
//Tony Build 2020.02.29

tcalendar.GetUtCDate = function(date){
    var now = new Date();
    if(date && date != ''){
        now = new Date(date);
    }
    var year = now.getUTCFullYear();
    var month = now.getUTCMonth() + 1;
    var day = now.getUTCDate();
    var a = [year,month,day];
    console.log('GetUtCDate',a);
    return a;
}
tcalendar.CreateUtC = function(y,m,d){
    var now = new Date(y,m-1,d);
    var year = now.getUTCFullYear();
    var month = now.getUTCMonth() + 1;
    var day = now.getUTCDate();
    var a = [year,month,day];
    console.log('CreateUtC',a);
    return a;
}
tcalendar.CreateLoc = function(y,m,d){
    console.log('CreateLoc',y,m,d);
    var now = new Date(y,m-1,d);
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var a = [year,month,day];
    console.log('CreateLoc',a);
    return a;
}
tcalendar.GetLocDate = function(date){
    var now = new Date();
    if(date && date != ''){
        now = new Date(date);
    }
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var a = [year,month,day];
    console.log('GetLocDate',a);
    return a;
}
tcalendar.daysInMonth = function(month, year){
  return new Date(year, month, 0).getDate();
}