﻿let lang_obj = {};
lang_obj["key"] = "uk_UA";
lang_obj["home"] = {
  "home" : "Повернутися",
  "calendar" : "Календар подій",
  "announce" : "Оголошення",
  "video" : "Відео",
  "related" : "Схожі сайти",
  "cookie" : "З метою надання кращого сервісу сайт використовує файли cookies. Якщо ви продовжите, ми будемо вважати, що ви погоджуєтесь отримувати файли cookies на всіх веб-сайтах taiwantrade.com.",
  "cookieurl" : "Умови Політики щодо файлів cookies",
  "newscenter" : "News Center",
  "recent" : "Recent News",
  "highlights" : "Highlights",
};
lang_obj["head"] = {
  "contackus" : "Зв'яжіться з нами",
  "search" : "Пошук",
  "more" : "Більше",
};
lang_obj["menu"] = {
  "taitra" : "Про Тайтра",
  "aboutus" : "Про нас",
  "aboutTaiwantrade" : "Про Тайвань Трейд",
  "contactus" : "Зв'яжіться з нами",
  "inquiry" : "Потреби для бізнесу",
  "aboutTaitra" : "Про Тайтра",
  "events" : "Події",
  "TTS" : "Виставки в Тайбеї",
  "conference" : "Список Конференц-залу",
  "news" : "Новини",
};
lang_obj["contact"] = {
  "name" : "Ім'я",
  "manager" : "Director",
  "title" : "Назва",
  "phone" : "Телефон",
  "ext" : "Ext.",
  "mobile" : "Mobile",
  "fax" : "Факс",
  "email" : "Електронна пошта",
  "emailerror" : "Please fill in the standard format for email, for example: abc@yahxx.com",
  "addr" : "Адреса",
};
lang_obj["back"] = "Назад";
lang_obj["show"] = {
  "name" : "Name of Exhibition",
  "date" : "Exhibition Date",
};
lang_obj["form"] = {
  "mission" : "Trade Mission",
  "langcode" : "Language Code",
  "need" : "Must be entered",
  "lang" : "Language",
  "select" : "Обиріть",
  "cancel" : "Відминити",
  "msg" : "Ваша заява була відправлена і буде розглянута так швидко, як це буде можливо. Дякуємо вам. ",
  "inProcess" : "Ваш запит розглядається ",
  "require" : "Потрібне поле",
  "info" : "Контактна інформація",
  "fill" : "Будь-ласка заповніть всі поля",
  "offer" : "Тип Транзакції",
  "buy" : "Купити",
  "sell" : "Продати",
  "cname" : "Назва Компанії",
  "name" : "Ім'я",
  "title" : "Назва",
  "contry" : "Країна",
  "state" : "State/Province",
  "addr" : "Адреса Компанії",
  "email" : "Єлектронна пошта",
  "web" : "Вебсайт",
  "tel" : "Телефон",
  "ext" : "Ext.",
  "fax" : "Факс",
  "btyle" : "Тип Бізнесу",
  "product" : "Інформація по продукту",
  "provide" : "(Будь ласка зробіть детальний опис продукту щоб ми змогли порекомендувати підходящих постачальників)",
  "pname" : "Назва продукту",
  "desc" : "Опис/Специфікація",
  "amout" : "Годовий розмір закупівель (USD)",
  "commet" : "Коментарі",
  "updfile" : "Завантажити документ",
  "next" : "Next",
  "submit" : "Відпавити",
  "reset" : "Вийти",
  "vscode" : "Код перевірки",
  "subject" : "Subject",
  "bno" : "Unified Business No.",
  "only" : "for Taiwan company only",
  "okmsg" : "Дякуємо за ваше подання ",
  "isclear" : "Ві впевнені що хочете очистити список закупок ?",
  "upload" : "Завантажити документ",
  "bnoerror" : "Unified Business No can only be an integer",
  "bnolength" : "Unified Business No cannot be lower than 8 words",
  "countrycode" : "Telephone (country code)",
  "areacode" : "Telephone (area code)",
  "dateerror" : "Дата закінчення повинна бути пізніше ніж дата початку",
  "cmtmsg" : "Коментарі",
  "notice" : "Будь ласка заповніть наступну форму, щоб ми змогли обробити ваш запит швидко та корректно. ",
  "rpeople" : "Число потрібних людей",
  "mustjoin" : "Мінімальне відвудування",
  "noData" : "No data available at this time.",
  "ok" : "Ok",
  "emailerror" : "Sorry, the Email address you typed does not seem correct, please check it again.",
  "check" : "Попередній перегляд",
  "postcode" : "Postcode",
  "signdate" : "Дата регістрації",
  "nameRqr" : "You forgot to fill the field 'Ім'я' - please fill it.",
  "countryRqr" : "You forgot to select the field 'Країна' - please select it.",
  "mailRqr" : "You forgot to fill the field 'Єлектронна пошта' - please fill it.",
  "subjectRqr" : "You forgot to fill the field 'Subject' - please fill it.",
  "otherRqr" : "You forgot to fill the field 'Коментарі' - please fill it.",
  "checkRqr" : "You forgot to fill the field 'Verification Code' - please fill it.",
  "checkValid" : "Verification Code is illegal.",
  "buyerValid" : "You forgot to select the field 'Тип Транзакції' - please select it.",
  "companyRqr" : "You forgot to fill the field 'Назва Компанії' - please fill it.",
  "countrySelect" : "Country must select.",
  "stateRqr" : "You forgot to fill the field 'State/Province' - please fill it.",
  "caddrRqr" : "You forgot to fill the field 'Адреса Компанії' - please fill it.",
  "emailRqr" : "You forgot to fill the field 'Єлектронна пошта' - please fill it.",
  "webRqr" : "You forgot to fill the field 'Вебсайт' - please fill it.",
  "ccodeRqr" : "You forgot to fill the field 'Telephone (country code)' - please fill it.",
  "areacodeRqr" : "You forgot to fill the field 'Telephone (area code)' - please fill it.",
  "phoneRqr" : "You forgot to fill the field 'Телефон' - please fill it.",
  "dealSelect" : "You forgot to select the field 'Тип Бізнесу' - please select it.",
  "pnameRqr" : "You forgot to fill the field 'Назва продукту' - please fill it.",
  "review" : "Please review your information, then click Submit",
};
lang_obj["event"] = {
  "date" : "Час",
  "name" : "Предмет",
  "addr" : "Місце розташування",
  "fee" : "Плата за регістрацію",
  "attach" : "Додаток",
  "url" : "Сайт про подію",
  "type" : "Тип події",
  "signup" : "Підпишітся",
};
lang_obj["news"] = {
  "date" : "Date",
  "title" : "Title",
  "source" : "Source",
};
lang_obj["sitemap"] = "Карта сайту";
lang_obj["year"] = "Year";
lang_obj["startDate"] = "Дата початку";
lang_obj["btn"] = {
  "previous" : "Назад",
  "checkout" : "Перевірка",
};
lang_obj["cof"] = {
  "device" : "Місця для зустрічей",
};
lang_obj["footer"] = {
  "info" : "Рада щодо розвитку торгівлі Тайваню, всі права захищені",
};
lang_obj["ovs"] = {
  "ofs" : "Overseas Offices",
};
lang_obj["conf"] = {
  "idnoRqr" : "You forgot to fill the field 'Unified Business No.' - please fill it.",
  "unitRqr" : "You forgot to fill the field 'Unit/Company Name' - please fill it.",
  "ckdateRqr" : "Date must select.",
  "contactRqr" : "You forgot to fill the field 'Contact Person' - please fill it.",
  "titleRqr" : "You forgot to fill the field 'Title' - please fill it.",
  "phoneRqr" : "You forgot to fill the field 'Tel.' - please fill it.",
  "faxRqr" : "You forgot to fill the field 'Fax' - please fill it.",
  "mailRqr" : "You forgot to fill the field 'E-mail' - please fill it.",
  "actRqr" : "You forgot to fill the field 'Event Name' - please fill it.",
  "countRqr" : "You forgot to fill the field 'Expected Attendance' - please fill it.",
  "ok" : "Thanks for your participation",
  "step1" : "Choose Conference Room",
  "step2" : "Fill in Application",
  "step3" : "Preview",
  "step4" : "Done",
  "intro" : "Please complete the following form so we can process your application quickly and correctly.",
  "SelectRqr" : "Choose Conference Room: Required equipment",
  "device" : "Required equipment",
  "name" : "Name",
  "idno" : "Unified Business No.",
  "unit" : "Unit/Company Name",
  "cdate" : "Date",
  "period" : "Period (hour)",
  "contact" : "Contact Person",
  "title" : "Title",
  "addr" : "Address",
  "country" : "Country",
  "phone" : "Tel.",
  "fax" : "Fax",
  "mail" : "E-mail",
  "web" : "Web Site",
  "actname" : "Event Name",
  "count" : "Expected Attendance",
  "contactType" : "Contact Method",
  "email" : "E-mail",
  "note" : "Remark",
  "list" : "Conference Room List",
  "sign" : "Reserve conference room",
};
lang_obj["eventform"] = {
  "gender" : "Стать",
  "ln" : "Родинне ім'я",
  "fn" : "Перше ім'я",
};
lang_obj["add"] = {
  "success" : "Add Calendar Success",
  "fail" : "Add Calendar Fail",
};
export const UK_UA_TRANS = lang_obj;
